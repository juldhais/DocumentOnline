﻿using System;

namespace DocumentOnline.Models
{
    public class Notification
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public User User { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
    }
}