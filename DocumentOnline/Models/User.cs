﻿using System;

namespace DocumentOnline.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public User Parent { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string CompanyName { get; set; }
        public string BranchName { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Remarks { get; set; }
        public string DocumentFolder { get; set; }

        public bool AllowDownloadDocument { get; set; }
        public bool AllowDownloadDocumentDetail { get; set; }
        public bool AllowUploadDocument { get; set; }
        public bool AllowUploadDocumentDetail { get; set; }
        public bool AllowDownloadDocumentRecap { get; set; }
        public bool AllowUploadDocumentRecap { get; set; }

        public bool IsActive { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public Guid? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
    }
}