﻿using System;

namespace DocumentOnline.Models
{
    public class DocumentRecap
    {
        public Guid Id { get; set; }
        public Guid? FromUserId { get; set; }
        public User FromUser { get; set; }
        public Guid? ToUserId { get; set; }
        public User ToUser { get; set; }
        public DateTime Date { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public int DownloadCount { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public Guid? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
    }
}