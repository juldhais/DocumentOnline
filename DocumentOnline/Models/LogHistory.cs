﻿using System;

namespace DocumentOnline.Models
{
    public class LogHistory
    {
        public Guid Id { get; set; }
        public User User { get; set; }
        public Guid? UserId { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public string IP { get; set; }
        public string Browser { get; set; }
    }
}