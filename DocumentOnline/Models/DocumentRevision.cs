﻿using System;

namespace DocumentOnline.Models
{
    public class DocumentRevision
    {
        public Guid Id { get; set; }
        public Guid? DocumentId { get; set; }
        public Document Document { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public string Remarks { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public Guid? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }

        public int DownloadCount { get; set; }
    }
}