﻿using System;

namespace DocumentOnline.Models
{
    public class Document
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public User User { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public string CustomerName { get; set; }
        public string ContractNumber { get; set; }
        public DateTime? ContractDate { get; set; }
        public string RegistrationNumber { get; set; }
        public string AktaNumber { get; set; }
        public DateTime? AktaDate { get; set; }
        public string SertifikatNumber { get; set; }
        public DateTime? SertifikatDate { get; set; }
        public string BillID { get; set; }
        public string Notaris { get; set; }

        public Guid? OpenById { get; set; }
        public User OpenBy { get; set; }
        public Guid? InProgressById { get; set; }
        public User InProgressBy { get; set; }
        public Guid? DoneById { get; set; }
        public User DoneBy { get; set; }
        public Guid? ReOpenById { get; set; }
        public User ReOpenBy { get; set; }
        public Guid? CancelById { get; set; }
        public User CancelBy { get; set; }

        public DateTime? OpenDate { get; set; }
        public DateTime? InProgressDate { get; set; }
        public DateTime? DoneDate { get; set; }
        public DateTime? ReOpenDate { get; set; }
        public DateTime? CancelDate { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public Guid? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }

        public int DownloadCount { get; set; }
    }
}