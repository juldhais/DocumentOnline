﻿using System;

namespace DocumentOnline.Models
{
    public class Invoice
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public User User { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string Remarks { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public string AttachmentFileName { get; set; }
        public string AttachmentSystemFileName { get; set; }
        public int DownloadCount { get; set; }
        public string Status { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public Guid? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
    }
}