﻿using System;

namespace DocumentOnline.Models
{
    public class DocumentType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public Guid? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
    }
}