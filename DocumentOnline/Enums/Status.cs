﻿namespace DocumentOnline.Enums
{
    public static class Status
    {
        public static readonly string Open = "Open";
        public static readonly string InProgress = "In-Progress";
        public static readonly string Done = "Done";
        public static readonly string ReOpen = "Re-Open";
        public static readonly string Cancel = "Cancel";
    }

    public static class InvoiceStatus
    {
        public static readonly string Open = "Open";
        public static readonly string Paid = "Paid";
    }
}