﻿namespace DocumentOnline.Enums
{
    public static class Role
    {
        public static readonly string SuperAdmin = "SuperAdmin";
        public static readonly string Admin = "Admin";
        public static readonly string SuperUser = "SuperUser";
        public static readonly string User = "User";
    }
}