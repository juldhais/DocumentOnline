﻿namespace DocumentOnline.Enums
{
    public static class DataType
    {
        public static readonly string String = "String";
        public static readonly string Integer = "Integer";
        public static readonly string Decimal = "Decimal";
        public static readonly string Date = "Date";
    }
}