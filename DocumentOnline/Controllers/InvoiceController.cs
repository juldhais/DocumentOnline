﻿using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class InvoiceController : Controller
    {
        private DataContext db;
        private InvoiceLogic invoiceLogic;
        private UserLogic userLogic;

        public InvoiceController()
        {
            db = new DataContext();
            invoiceLogic = new InvoiceLogic(db);
            userLogic = new UserLogic(db);
        }

        [AuthorizeUser]
        public ActionResult Index()
        {
            ViewBag.IsAdmin = this.IsAdmin();
            return View();
        }

        [HttpPost]
        [AuthorizeUser]
        public JsonResult GetData(DataTableParameter parameter)
        {
            var currentUser = this.GetCurrentUser();

            var result = invoiceLogic.GetListDataTable(currentUser.Id, parameter);

            return Json(result);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public ActionResult Upload()
        {
            ViewBag.IsAdmin = this.IsAdmin();
            ViewBag.UserList = userLogic.GetListSuperUserAndUser();
            return View();
        }

        [HttpPost]
        [AuthorizeUser("SuperAdmin", "Admin")]
        public JsonResult UploadFiles()
        {
            var result = new List<string>();

            if (Request.Files.Count == 0)
            {
                result.Add("Belum ada file yang dipilih");
                return Json(result);
            }

            var currentUser = this.GetCurrentUser();

            var userId = Request.Form.Get("UserId").ToGuid();
            var remarks = Request.Form.Get("Remarks").Stringify();

            var userDocumentFolder = "";
            if (userId.IsGuidNotEmpty())
            {
                var user = userLogic.Get(userId);
                if (user != null) userDocumentFolder = user.DocumentFolder;
            }
            else userDocumentFolder = currentUser.DocumentFolder;

            foreach (var item in Request.Files)
            {
                try
                {
                    var file = Request.Files[item.ToString()];

                    var fileName = Path.GetFileName(file.FileName);
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.FileName);
                    var systemFileName = FileHelper.CreateSystemFileName(file.FileName);

                    var invoice = new InvoiceViewModel();
                    invoice.Date = DateTime.Now;
                    invoice.OriginalFileName = fileName;
                    invoice.SystemFileName = systemFileName;
                    invoice.Remarks = remarks;
                    invoice.UserId = userId;
                    invoiceLogic.Create(invoice, currentUser);

                    var documentFolder = this.GetDocumentFolder(userDocumentFolder, invoice.Date.Year);
                    this.SaveUploadedFile(file, documentFolder, systemFileName);

                    db.SaveChanges();

                    result.Add($"Success: File {fileName} berhasil diupload.");
                }
                catch (Exception ex)
                {
                    result.Add("Failed: " + ex.Message);
                }
            }

            return Json(result);
        }

        [HttpGet]
        [AuthorizeUser]
        public FileResult DownloadFile(Guid id)
        {
            var invoice = invoiceLogic.Get(id);

            var currentUser = this.GetCurrentUser();

            if (!this.IsAdmin())
            {
                invoice.DownloadCount += 1;
                invoiceLogic.Update(invoice, currentUser);
                db.SaveChanges();
            }

            var path = this.GetDocumentPath
                        (
                            invoice.UserDocumentFolder,
                            invoice.Date.Year,
                            invoice.SystemFileName
                        );

            if (System.IO.File.Exists(path))
            {
                var extension = Path.GetExtension(invoice.SystemFileName).Replace(".", "");
                var result = File(path, extension, invoice.OriginalFileName);
                return result;
            }
            else return this.CreateNotFoundFile(invoice.OriginalFileName);
        }

        [HttpGet]
        [AuthorizeUser]
        public FileResult DownloadFiles(string ids)
        {
            var listId = ids.Split(';');
            var stream = new MemoryStream();
            var zip = new ZipFile();

            var currentUser = this.GetCurrentUser();
            var isAdmin = this.IsAdmin();

            foreach (var item in listId)
            {
                try
                {
                    var id = item.ToGuid();
                    if (id == null) continue;

                    var invoice = invoiceLogic.Get(id.Value);

                    if (!isAdmin)
                    {
                        invoice.DownloadCount += 1;
                        invoiceLogic.Update(invoice, currentUser);
                    }

                    var path = this.GetDocumentPath
                                (
                                    invoice.UserDocumentFolder,
                                    invoice.Date.Year,
                                    invoice.SystemFileName
                                );

                    if (System.IO.File.Exists(path))
                        zip.AddFile(path, "").FileName = invoice.OriginalFileName;
                    else zip.AddEntry(invoice.OriginalFileName + ".notfound.txt", "FILE NOT FOUND");
                }
                catch { }
            }

            db.SaveChanges();

            zip.Save(stream);
            stream.Position = 0;
            return File(stream, "application/zip", $"DocumentRecapDownload_{DateTime.Now.ToString("dd.MM.yyyy.HH.mm.ss")}.zip");
        }

        [HttpPost]
        [AuthorizeAjax("SuperAdmin")]
        public JsonResult DeleteInvoice(Guid? id)
        {
            try
            {
                invoiceLogic.Delete(id);
                db.SaveChanges();
                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json("Failed: " + ex.Message);
            }
        }
    }
}