﻿using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using System;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class UserManagementController : Controller
    {
        private DataContext db;
        private UserLogic userLogic;

        public UserManagementController()
        {
            db = new DataContext();
            userLogic = new UserLogic(db);
        }

        [AuthorizeUser("SuperUser", "SuperAdmin", "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeUser("SuperUser", "SuperAdmin", "Admin")]
        public JsonResult GetData(DataTableParameter parameter)
        {
            var currentUser = Session["CurrentUser"] as UserViewModel;

            var result = userLogic.GetListUserDataTable(currentUser, parameter);

            return Json(result);
        }

        [AuthorizeUser("SuperUser", "SuperAdmin", "Admin")]
        public ActionResult Create()
        {
            var currentUser = this.GetCurrentUser();
            ViewBag.IsAdmin = this.IsAdmin();

            var model = new UserViewModel();

            if (currentUser.Role == Role.SuperUser)
                model.CompanyName = currentUser.CompanyName;

            return View(model);
        }

        [HttpPost]
        [AuthorizeUser("SuperUser", "SuperAdmin", "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var currentUser = this.GetCurrentUser();

                    if (userViewModel.Role.IsEmpty())
                        userViewModel.Role = Role.User;

                    if (currentUser.Role == Role.SuperUser)
                        userViewModel.ParentId = currentUser.Id;

                    userLogic.Create(userViewModel, currentUser);

                    db.SaveChanges();

                    ViewBag.Message = "Data user berhasil disimpan.";

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                throw;
            }

            return View(userViewModel);
        }

        [AuthorizeUser("SuperUser", "SuperAdmin", "Admin")]
        public ActionResult Update(Guid? id)
        {
            ViewBag.IsAdmin = this.IsAdmin();
            var user = userLogic.Get(id);
            user.Password = EncryptHelper.Decrypt(user.Password);

            return View(user);
        }

        [HttpPost]
        [AuthorizeUser("SuperUser", "SuperAdmin", "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var currentUser = Session["CurrentUser"] as UserViewModel;

                    userLogic.Update(userViewModel, currentUser);

                    db.SaveChanges();

                    ViewBag.Message = "Data user berhasil disimpan.";

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                throw;
            }

            return View(userViewModel);
        }

        [AuthorizeUser("SuperUser", "SuperAdmin", "Admin")]
        public JsonResult Delete(Guid? id)
        {
            try
            {
                var currentUser = Session["CurrentUser"] as UserViewModel;

                userLogic.Delete(id, currentUser);

                db.SaveChanges();

                return Json("Data user berhasil dihapus.", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}