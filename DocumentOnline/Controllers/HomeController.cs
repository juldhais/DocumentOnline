﻿using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class HomeController : Controller
    {
        private DataContext db;
        private UserLogic userLogic;
        private DocumentTypeLogic docTypeLogic;
        private LogHistoryLogic logHistoryLogic;

        public HomeController()
        {
            db = new DataContext();
            EntityValidator.DbValidator.Validate(db);

            userLogic = new UserLogic(db);
            docTypeLogic = new DocumentTypeLogic(db);
            logHistoryLogic = new LogHistoryLogic(db);

            CreateDefaultUserAdmin();
            CreateDefaultDocType();
        }

        [AuthorizeUser]
        public ActionResult Index()
        {
            var currentUser = this.GetCurrentUser();

            if (currentUser.Role == Role.SuperUser)
                return RedirectToAction("Index", "UserDashboard");
            else if (currentUser.Role == Role.User)
                return RedirectToAction("Index", "UserDashboard");
            else if (currentUser.Role == Role.SuperAdmin)
                return RedirectToAction("Index", "AdminDashboard");
            else if (currentUser.Role == Role.Admin)
                return RedirectToAction("Index", "AdminDashboard");
            else return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel viewModel)
        {
            try
            {
                var currentUser = userLogic.ValidateLogin(viewModel.UserName, viewModel.Password);

                var log = new LogHistoryViewModel();
                log.UserId = currentUser.Id;
                log.LoginDate = DateTime.Now;
                log.IP = Request.UserHostAddress;
                log.Browser = Request.Browser?.Browser;
                logHistoryLogic.Create(log);
                db.SaveChanges();
                
                Session["CurrentUser"] = currentUser;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            return View(viewModel);
        }

        [AuthorizeUser]
        public ActionResult Logout()
        {
            var currentUser = this.GetCurrentUser();
            var log = logHistoryLogic.GetLastLogin(currentUser.Id);
            if (log != null)
            {
                log.LogoutDate = DateTime.Now;
                logHistoryLogic.Update(log);
                db.SaveChanges();
            }

            Session.Clear();
            return RedirectToAction("Login");
        }

        [AuthorizeUser]
        public ActionResult UnauthorizedUser()
        {
            return View();
        }

        private void CreateDefaultUserAdmin()
        {
            try
            {
                if (db.Users.Any()) return;

                var admin = new UserViewModel();
                admin.Name = "admin";
                admin.Password = "admin";
                admin.Role = "SuperAdmin";
                userLogic.Create(admin, null);

                var user = new UserViewModel();
                user.Name = "user";
                user.Password = "user";
                user.Role = "SuperUser";
                user.DocumentFolder = "USER";
                userLogic.Create(user, null);

                db.SaveChanges();
            }
            catch { }
        }

        private void CreateDefaultDocType()
        {
            try
            {
                if (db.DocumentTypes.Any()) return;

                var akta = new DocumentTypeViewModel();
                akta.Name = "AKTA";
                docTypeLogic.Create(akta, null);

                var sjf = new DocumentTypeViewModel();
                sjf.Name = "SJF";
                docTypeLogic.Create(sjf, null);

                var lampiran = new DocumentTypeViewModel();
                lampiran.Name = "LAMPIRAN";
                docTypeLogic.Create(lampiran, null);

                var pernyataan = new DocumentTypeViewModel();
                pernyataan.Name = "PERNYATAAN";
                docTypeLogic.Create(pernyataan, null);

                var buktiPesan = new DocumentTypeViewModel();
                buktiPesan.Name = "BUKTIBAYAR";
                docTypeLogic.Create(buktiPesan, null);

                db.SaveChanges();
            }
            catch { }
        }
    }
}