﻿using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using System;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class DocumentTypeController : Controller
    {
        private DataContext db;
        private DocumentTypeLogic docTypeLogic;

        public DocumentTypeController()
        {
            db = new DataContext();
            docTypeLogic = new DocumentTypeLogic(db);
        }

        [AuthorizeUser("SuperAdmin")]
        public ActionResult Index()
        {
            var list = docTypeLogic.GetList();
            return View(list);
        }

        [AuthorizeUser("SuperAdmin")]
        public ActionResult Create()
        {
            return View(new DocumentTypeViewModel());
        }

        [HttpPost]
        [AuthorizeUser("SuperAdmin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DocumentTypeViewModel docType)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Data tidak valid.");

                if (docType.Name.IsEmpty())
                    throw new Exception("Nama tidak boleh kosong.");

                docTypeLogic.Create(docType, this.GetCurrentUser());
                db.SaveChanges();

                ViewBag.Message = "Data berhasil disimpan.";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View(docType);
            }
        }

        [AuthorizeUser("SuperAdmin")]
        public ActionResult Update(Guid? id)
        {
            try
            {
                var docType = docTypeLogic.Get(id);

                return View(docType);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [AuthorizeUser("SuperAdmin")]
        [ValidateAntiForgeryToken]
        public ActionResult Update(DocumentTypeViewModel docType)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Data tidak valid.");

                if (docType.Name.IsEmpty())
                    throw new Exception("Nama tidak boleh kosong.");

                docTypeLogic.Update(docType, this.GetCurrentUser());
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View(docType);
            }
        }
    }
}