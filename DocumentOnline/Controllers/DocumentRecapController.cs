﻿using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class DocumentRecapController : Controller
    {
        private DataContext db;
        private DocumentRecapLogic docRecapLogic;
        private UserLogic userLogic;

        public DocumentRecapController()
        {
            db = new DataContext();
            docRecapLogic = new DocumentRecapLogic(db);
            userLogic = new UserLogic(db);
        }

        [AuthorizeUser]
        public ActionResult Index()
        {
            ViewBag.IsAdmin = this.IsAdmin();
            return View();
        }

        [HttpPost]
        [AuthorizeUser]
        public JsonResult GetData(DataTableParameter parameter)
        {
            var currentUser = this.GetCurrentUser();

            var result = docRecapLogic.GetListDataTable(currentUser.Id, parameter);

            return Json(result);
        }
        
        [AuthorizeUser]
        public ActionResult Upload()
        {
            ViewBag.IsAdmin = this.IsAdmin();
            ViewBag.UserList = userLogic.GetListSuperUserAndUser();
            return View();
        }

        [HttpPost]
        [AuthorizeUser]
        public JsonResult UploadFiles()
        {
            var result = new List<string>();

            if (Request.Files.Count == 0)
            {
                result.Add("Belum ada file yang dipilih");
                return Json(result);
            }

            var currentUser = this.GetCurrentUser();

            var toUserId = Request.Form.Get("ToUserId").ToGuid();
            var userDocumentFolder = "";
            if (toUserId.IsGuidNotEmpty())
            {
                var user = userLogic.Get(toUserId);
                if (user != null) userDocumentFolder = user.DocumentFolder;
            }
            else userDocumentFolder = currentUser.DocumentFolder;

            foreach (var item in Request.Files)
            {
                try
                {
                    var file = Request.Files[item.ToString()];

                    var fileName = Path.GetFileName(file.FileName);
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.FileName);
                    var systemFileName = FileHelper.CreateSystemFileName(file.FileName);

                    var docRecap = new DocumentRecapViewModel();
                    docRecap.Date = DateTime.Now;
                    docRecap.OriginalFileName = fileName;
                    docRecap.SystemFileName = systemFileName;
                    docRecap.FromUserId = currentUser.Id;
                    docRecap.ToUserId = toUserId;
                    docRecapLogic.Create(docRecap, currentUser);

                    var documentFolder = this.GetDocumentFolder(userDocumentFolder, docRecap.Date.Year);
                    this.SaveUploadedFile(file, documentFolder, systemFileName);

                    db.SaveChanges();

                    result.Add($"Success: File {fileName} berhasil diupload.");
                }
                catch (Exception ex)
                {
                    result.Add("Failed: " + ex.Message);
                }
            }
            
            return Json(result);
        }

        [HttpGet]
        [AuthorizeUser]
        public FileResult DownloadFile(Guid id)
        {
            var docRecap = docRecapLogic.Get(id);

            var currentUser = this.GetCurrentUser();

            docRecap.DownloadCount += 1;
            docRecapLogic.Update(docRecap, currentUser);
            db.SaveChanges();

            var path = this.GetDocumentPath
                        (
                            docRecap.ToUserDocumentFolder,
                            docRecap.Date.Year,
                            docRecap.SystemFileName
                        );

            if (System.IO.File.Exists(path))
            {
                var extension = Path.GetExtension(docRecap.SystemFileName).Replace(".", "");
                var result = File(path, extension, docRecap.OriginalFileName);
                return result;
            }
            else return this.CreateNotFoundFile(docRecap.OriginalFileName);
        }

        [HttpGet]
        [AuthorizeUser]
        public FileResult DownloadFiles(string ids)
        {
            var listId = ids.Split(';');
            var stream = new MemoryStream();
            var zip = new ZipFile();

            var currentUser = this.GetCurrentUser();

            foreach (var item in listId)
            {
                try
                {
                    var id = item.ToGuid();
                    if (id == null) continue;

                    var docRecap = docRecapLogic.Get(id.Value);
                    docRecap.DownloadCount += 1;
                    docRecapLogic.Update(docRecap, currentUser);

                    var path = this.GetDocumentPath
                                (
                                    docRecap.ToUserDocumentFolder,
                                    docRecap.Date.Year,
                                    docRecap.SystemFileName
                                );

                    if (System.IO.File.Exists(path))
                        zip.AddFile(path, "").FileName = docRecap.OriginalFileName;
                    else zip.AddEntry(docRecap.OriginalFileName + ".notfound.txt", "FILE NOT FOUND");
                }
                catch { }
            }

            db.SaveChanges();

            zip.Save(stream);
            stream.Position = 0;
            return File(stream, "application/zip", $"DocumentRecapDownload_{DateTime.Now.ToString("dd.MM.yyyy.HH.mm.ss")}.zip");
        }


    }
}