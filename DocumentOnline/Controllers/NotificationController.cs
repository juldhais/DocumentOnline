﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DocumentOnline.ViewModels;
using DocumentOnline.Logics;
using DocumentOnline.Helpers;

namespace DocumentOnline.Controllers
{
    public class NotificationController : Controller
    {
        private DataContext db;
        private NotificationLogic notificationLogic;

        public NotificationController()
        {
            db = new DataContext();
            notificationLogic = new NotificationLogic(db);
        }

        public ActionResult Index()
        {
            return new HttpUnauthorizedResult();
        }

        [AuthorizeUser]
        public JsonResult Get()
        {
            try
            {
                var currentUser = this.GetCurrentUser();

                var notificationList = notificationLogic.GetListStringUnreadNotification(currentUser);

                var result = new
                {
                    Count = notificationList.Count,
                    Data = notificationList
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            
            
        }

        [AuthorizeUser]
        public JsonResult Clear(Guid? notificationId)
        {
            try
            {
                notificationLogic.Read(notificationId);

                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }
    }
}