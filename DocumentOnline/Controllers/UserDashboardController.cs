﻿using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class UserDashboardController : Controller
    {
        private DataContext db;
        private DocumentLogic documentLogic;
        private DocumentDetailLogic documentDetailLogic;
        private DocumentRevisionLogic documentRevisionLogic;
        private NotificationLogic notificationLogic;

        private const int MAX_FILE_SIZE = 20 * 1024 * 1024;
        private const string ALLOWED_FILE_TYPE = ".pdf .docx .doc";

        public UserDashboardController()
        {
            db = new DataContext();
            documentLogic = new DocumentLogic(db);
            documentDetailLogic = new DocumentDetailLogic(db);
            documentRevisionLogic = new DocumentRevisionLogic(db);
            notificationLogic = new NotificationLogic(db);
        }

        [AuthorizeUser("SuperUser", "User")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeUser("SuperUser", "User")]
        public JsonResult GetData(DataTableParameter parameter)
        {
            var currentUser = Session["CurrentUser"] as UserViewModel;

            var result = documentLogic.GetListDataTable(currentUser.Id, parameter);

            return Json(result);
        }

        [HttpGet]
        [AuthorizeUser("SuperUser", "User")]
        public FileResult DownloadFile(Guid id)
        {
            var document = documentLogic.Get(id);

            var path = this.GetDocumentPath
                        (
                            document.UserDocumentFolder,
                            document.Date.Year, 
                            document.SystemFileName
                        );

            if (System.IO.File.Exists(path))
            {
                var extension = Path.GetExtension(document.SystemFileName).Replace(".", "");
                var result = File(path, extension, document.OriginalFileName);
                return result;
            }
            else return this.CreateNotFoundFile(document.OriginalFileName);
        }

        [HttpGet]
        [AuthorizeUser("SuperUser", "User")]
        public FileResult DownloadDetail(Guid id, string docType)
        {
            var docDetail = documentDetailLogic.Get(id, docType);
            docDetail.DownloadCount += 1;
            documentDetailLogic.Update(docDetail, this.GetCurrentUser());
            db.SaveChanges();

            var path = this.GetDocumentPath
                        (
                            docDetail.UserDocumentFolder,
                            docDetail.DocumentDate.Year,
                            docDetail.SystemFileName
                        );

            if (System.IO.File.Exists(path))
            {
                var extension = Path.GetExtension(docDetail.SystemFileName).Replace(".", "");
                var result = File(path, extension, docDetail.OriginalFileName);
                return result;
            }
            else return this.CreateNotFoundFile(docDetail.OriginalFileName);
        }

        [HttpGet]
        [AuthorizeUser("SuperUser", "User")]
        public FileResult DownloadDetails(string ids)
        {
            try
            {
                var listId = ids.Split(';');
                var stream = new MemoryStream();
                var zip = new ZipFile();

                var currentUser = this.GetCurrentUser();

                foreach (var item in listId)
                {
                    var id = item.ToGuid();
                    if (id == null) continue;

                    var docDetailList = documentDetailLogic.GetList(id);
                    foreach (var docDetail in docDetailList)
                    {
                        docDetail.DownloadCount += 1;
                        documentDetailLogic.Update(docDetail, currentUser);

                        var path = this.GetDocumentPath
                                    (
                                        docDetail.UserDocumentFolder, 
                                        docDetail.DocumentDate.Year,
                                        docDetail.SystemFileName
                                    );

                        if (System.IO.File.Exists(path))
                            zip.AddFile(path, "").FileName = docDetail.OriginalFileName;
                        else zip.AddEntry(docDetail.OriginalFileName + ".notfound.txt", "FILE NOT FOUND");
                    }
                }

                db.SaveChanges();
                
                zip.Save(stream);
                stream.Position = 0;
                return File(stream, "application/zip", $"DocumentDetailDownload_{DateTime.Now.ToString("dd.MM.yyyy.HH.mm.ss")}.zip");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return null;
            }
        }

        [AuthorizeUser("SuperUser", "User")]
        public ActionResult Upload()
        {
            ViewBag.AllowedFileType = ALLOWED_FILE_TYPE;
            return View();
        }

        [HttpPost]
        [AuthorizeUser("SuperUser", "User")]
        public JsonResult UploadFiles()
        {
            var result = new List<string>();

            if (Request.Files.Count == 0)
            {
                result.Add("Belum ada file yang dipilih");
                return Json(result);
            }

            var currentUser = this.GetCurrentUser();

            foreach (var item in Request.Files)
            {
                try
                {
                    var file = Request.Files[item.ToString()];
                    var fileName = Path.GetFileName(file.FileName);
                    var systemFileName = FileHelper.CreateSystemFileName(file.FileName);

                    ValidateUploadedFile(file);

                    var document = SaveDocument(fileName, systemFileName, currentUser);

                    var documentFolder = this.GetDocumentFolder(currentUser.DocumentFolder, DateTime.Now.Year);
                    this.SaveUploadedFile(file, documentFolder, systemFileName);

                    CreateNotification(document);

                    result.Add("Success: " + fileName + " #" + document.Number);
                }
                catch (Exception ex)
                {
                    result.Add("Failed: " + ex.Message);
                }
            }

            return Json(result);
        }

        private void ValidateUploadedFile(HttpPostedFileBase file)
        {
            if (file == null)
                throw new Exception("File kosong.");

            if (file.ContentLength == 0)
                throw new Exception("Belum ada file yang di upload.");

            var fileName = Path.GetFileName(file.FileName);

            if (file.ContentLength > MAX_FILE_SIZE)
                throw new Exception(fileName + " - Ukuran file tidak boleh lebih dari 10 MB.");

            var fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!ALLOWED_FILE_TYPE.Split(' ').Contains(fileExtension))
                throw new Exception(fileName + " - Tipe file yang diupload harus " + ALLOWED_FILE_TYPE);

            if (GetSplitedFileName(file.FileName).Count() < 2)
                throw new Exception(fileName + " - Format nama file harus: NOKONTRAK_NAMACUSTOMER");
        }

        private DocumentViewModel SaveDocument(string fileName, string systemFileName, UserViewModel currentUser)
        {
            var splitedFileName = GetSplitedFileName(fileName);
            var contractNumber = splitedFileName[0];
            var customerName = splitedFileName[1];

            if (db.Documents.Any(x => x.ContractNumber == contractNumber))
                throw new Exception($"Nomor kontrak {contractNumber} sudah ada.");

            var documentViewModel = new DocumentViewModel();
            documentViewModel.ContractNumber = contractNumber;
            documentViewModel.CustomerName = customerName;
            documentViewModel.Number = documentLogic.CreateDocumentNumber();
            documentViewModel.Status = Status.Open;
            documentViewModel.Date = DateTime.Now;
            documentViewModel.UserId = currentUser.Id;
            documentViewModel.OriginalFileName = fileName;
            documentViewModel.SystemFileName = systemFileName;
            documentViewModel.OpenById = currentUser.Id;
            documentViewModel.OpenDate = DateTime.Now;

            documentLogic.Create(documentViewModel, currentUser);

            db.SaveChanges();

            return documentViewModel;
        }

        private void CreateNotification(DocumentViewModel document)
        {
            var notification = new NotificationViewModel();
            notification.UserId = null; //for all admin
            notification.Date = DateTime.Now;
            notification.Message = $@"New document: {document.OriginalFileName}";
            notificationLogic.Create(notification);
            db.SaveChanges();
        }

        private string[] GetSplitedFileName(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName).Split('_');
        }

        [HttpPost]
        [AuthorizeAjax("SuperUser")]
        public JsonResult DeleteDocument(Guid? id)
        {
            try
            {
                var document = documentLogic.Get(id);
                if (document.Status != Status.Open)
                    throw new Exception($"Dokumen No: {document.ContractNumber} tidak dapat dihapus.");

                documentLogic.Delete(id);
                db.SaveChanges();
                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json("Failed: " + ex.Message);
            }
        }

        [AuthorizeUser("SuperUser", "User")]
        public ActionResult UploadRevision()
        {
            ViewBag.AllowedFileType = ALLOWED_FILE_TYPE;
            return View();
        }

        [HttpPost]
        [AuthorizeUser("SuperUser", "User")]
        public JsonResult UploadRevisionFiles()
        {
            var result = new List<string>();

            if (Request.Files.Count == 0)
            {
                result.Add("Belum ada file yang dipilih");
                return Json(result);
            }

            var currentUser = this.GetCurrentUser();

            foreach (var item in Request.Files)
            {
                try
                {
                    var file = Request.Files[item.ToString()];
                    var fileName = Path.GetFileName(file.FileName);
                    var systemFileName = FileHelper.CreateSystemFileName(file.FileName);

                    //validate file type
                    var allowedFileType = ".pdf .jpg .png .doc .docx";
                    var fileExtension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedFileType.Split(' ').Contains(fileExtension))
                        throw new Exception(fileName + " - Tipe file yang diupload harus " + allowedFileType);
                    
                    var splitedFileName = GetSplitedFileName(fileName);
                    var contractNumber = splitedFileName[0];
                    var remarks = splitedFileName[1];

                    var document = documentLogic.GetByContractNumber(contractNumber);
                    if (document == null) throw new Exception($"Dokumen {contractNumber} tidak ditemukan.");

                    var docRevision = new DocumentRevisionViewModel();
                    docRevision.DocumentId = document.Id;
                    docRevision.OriginalFileName = fileName;
                    docRevision.SystemFileName = systemFileName;
                    docRevision.Remarks = remarks;
                    documentRevisionLogic.Create(docRevision, currentUser);
                    db.SaveChanges();
                    
                    var documentFolder = this.GetDocumentFolder(currentUser.DocumentFolder, document.Date.Year);
                    this.SaveUploadedFile(file, documentFolder, systemFileName);
                    
                    result.Add("Success: " + fileName + " #" + document.Number);
                }
                catch (Exception ex)
                {
                    result.Add("Failed: " + ex.Message);
                }
            }

            return Json(result);
        }

        [HttpGet]
        [AuthorizeUser("SuperUser", "User")]
        public FileResult DownloadRevision(Guid id)
        {
            var docRevision = documentRevisionLogic.Get(id);

            var path = this.GetDocumentPath
                        (
                            docRevision.UserDocumentFolder,
                            docRevision.DocumentDate.Year,
                            docRevision.SystemFileName
                        );

            if (System.IO.File.Exists(path))
            {
                var extension = Path.GetExtension(docRevision.SystemFileName).Replace(".", "");
                var result = File(path, extension, docRevision.OriginalFileName);
                return result;
            }
            else return this.CreateNotFoundFile(docRevision.OriginalFileName);
        }
    }
}