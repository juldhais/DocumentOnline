﻿using DocumentOnline.Enums;
using DocumentOnline.ViewModels;
using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public static class ControllerExtensions
    {
        private static readonly string DOCUMENT_PATH = "~/App_Data/Document";

        public static UserViewModel GetCurrentUser(this Controller controller)
        {
            var currentUser = controller.Session["CurrentUser"] as UserViewModel;

            return currentUser;
        }

        public static bool IsAdmin(this Controller controller)
        {
            var currentUser = controller.GetCurrentUser();

            if (currentUser.Role == Role.Admin || currentUser.Role == Role.SuperAdmin)
                return true;
            else return false;
        }

        public static string GetDocumentFolder(this Controller controller, string userDocumentFolder, int year)
        {
            var path = controller.Server.MapPath(DOCUMENT_PATH + "/" + userDocumentFolder + "/" + year);
            return path;
        }

        public static string GetDocumentPath(this Controller controller, string userDocumentFolder, int year, string systemFileName)
        {
            var docFolder = controller.GetDocumentFolder(userDocumentFolder, year);
            var path = Path.Combine(docFolder, systemFileName);
            return path;
        }

        public static void SaveUploadedFile(this Controller controller, HttpPostedFileBase file, string folderPath, string systemFileName)
        {
            var fileName = Path.GetFileName(file.FileName);

            try
            {
                Directory.CreateDirectory(folderPath);
                var filePath = Path.Combine(folderPath, systemFileName);
                file.SaveAs(filePath);
            }
            catch (Exception ex)
            {
                throw new Exception($"Gagal menyimpan file {fileName} - {ex.Message}");
            }

        }

        public static FileResult CreateNotFoundFile(this Controller controller, string originalFileName)
        {
            byte[] content = Encoding.ASCII.GetBytes("FILE NOT FOUND");
            var result = new FileContentResult(content, "txt");
            result.FileDownloadName = originalFileName + ".notfound.txt";
            return result;
        }
    }
}