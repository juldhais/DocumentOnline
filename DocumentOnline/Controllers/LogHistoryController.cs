﻿using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class LogHistoryController : Controller
    {
        DataContext db;
        LogHistoryLogic logHistoryLogic;

        public LogHistoryController()
        {
            db = new DataContext();
            logHistoryLogic = new LogHistoryLogic(db);
        }

        [AuthorizeUser]
        public ActionResult Index()
        {
            ViewBag.Date = DateTime.Today.ToString("yyyy-MM-dd");
            var list = logHistoryLogic.GetList(DateTime.Today);
            return View(list);
        }

        [AuthorizeUser]
        public ActionResult Filter(DateTime? date)
        {
            if (date == null) date = DateTime.Today;

            ViewBag.Date = date.Value.ToString("yyyy-MM-dd");

            var list = logHistoryLogic.GetList(date);
            return View("Index", list);
        }
    }
}