﻿using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class AdminDashboardController : Controller
    {
        private DataContext db;
        private DocumentLogic documentLogic;
        private DocumentDetailLogic documentDetailLogic;
        private DocumentRevisionLogic documentRevisionLogic;
        private DocumentTypeLogic docTypeLogic;

        private const int MAX_FILE_SIZE = 20 * 1024 * 1024;
        private const string ALLOWED_FILE_TYPE = ".pdf .doc .docx .xls .xlsx";
        private const string EXCEL_IMPORT_PATH = "~/App_Data/ExcelImport";

        private const string DEFAULT_DOC_TYPE = "SJF";

        public AdminDashboardController()
        {
            db = new DataContext();
            documentLogic = new DocumentLogic(db);
            documentDetailLogic = new DocumentDetailLogic(db);
            documentRevisionLogic = new DocumentRevisionLogic(db);
            docTypeLogic = new DocumentTypeLogic(db);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeUser("SuperAdmin", "Admin")]
        public JsonResult GetData(DataTableParameter parameter)
        {
            var currentUser = this.GetCurrentUser();

            var result = documentLogic.GetListDataTable(currentUser.Id, parameter);

            return Json(result);
        }

        [HttpGet]
        [AuthorizeUser("SuperAdmin", "Admin")]
        public FileResult DownloadFile(Guid id)
        {
            var document = documentLogic.Get(id);

            if (document.Status == Status.Open || document.Status == Status.ReOpen)
            {
                var currentUser = this.GetCurrentUser();

                if (document.Status == Status.Open || document.Status == Status.ReOpen)
                    document.Status = Status.InProgress;
                document.InProgressById = currentUser.Id;
                document.InProgressDate = DateTime.Now;
                document.DownloadCount += 1;
                documentLogic.Update(document, currentUser);
                db.SaveChanges();
            }

            var path = this.GetDocumentPath
                        (
                            document.UserDocumentFolder,
                            document.Date.Year,
                            document.SystemFileName
                        );

            if (System.IO.File.Exists(path))
            {
                var extension = Path.GetExtension(document.SystemFileName).Replace(".", "");
                var result = File(path, extension, document.OriginalFileName);
                return result;
            }
            else return this.CreateNotFoundFile(document.OriginalFileName);
        }

        [HttpGet]
        [AuthorizeUser("SuperAdmin", "Admin")]
        public FileResult DownloadFiles(string ids)
        {
            try
            {
                var listId = ids.Split(';');
                var stream = new MemoryStream();
                var zip = new ZipFile();

                var currentUser = this.GetCurrentUser();

                foreach (var item in listId)
                {
                    try
                    {
                        var id = item.ToGuid();
                        if (id == null) continue;

                        var document = documentLogic.Get(id.Value);
                        if (document.Status == Status.Open || document.Status == Status.ReOpen)
                            document.Status = Status.InProgress;
                        document.InProgressById = currentUser.Id;
                        document.InProgressDate = DateTime.Now;
                        document.DownloadCount += 1;
                        documentLogic.Update(document, currentUser);

                        var path = this.GetDocumentPath
                                    (
                                        document.UserDocumentFolder,
                                        document.Date.Year,
                                        document.SystemFileName
                                    );

                        if (System.IO.File.Exists(path))
                            zip.AddFile(path, "").FileName = document.OriginalFileName;
                        else zip.AddEntry(document.OriginalFileName + ".notfound.txt", "FILE NOT FOUND");
                    }
                    catch { }
                }

                db.SaveChanges();

                zip.Save(stream);
                stream.Position = 0;
                return File(stream, "application/zip", $"DocumentDownload_{DateTime.Now.ToString("dd.MM.yyyy.HH.mm.ss")}.zip");
            }
            catch (Exception ex)
            {
                byte[] bytes = Encoding.ASCII.GetBytes(ex.Message);
                return File(bytes, "text/plain");
            }
        }

        [HttpGet]
        [AuthorizeUser("SuperAdmin", "Admin")]
        public FileResult DownloadDetail(Guid id, string docType)
        {
            var docDetail = documentDetailLogic.Get(id, docType);

            var path = this.GetDocumentPath
                        (
                            docDetail.UserDocumentFolder,
                            docDetail.DocumentDate.Year,
                            docDetail.SystemFileName
                        );

            var extension = Path.GetExtension(docDetail.SystemFileName).Replace(".", "");

            var result = File(path, extension, docDetail.OriginalFileName);

            if (System.IO.File.Exists(result.FileName))
                return result;
            else return this.CreateNotFoundFile(docDetail.OriginalFileName);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public ActionResult UploadDetail()
        {
            ViewBag.AllowedFileType = ALLOWED_FILE_TYPE;
            return View();
        }

        [HttpPost]
        [AuthorizeAjax("SuperAdmin", "Admin")]
        public JsonResult UploadDetailFiles()
        {
            var result = new List<string>();

            if (Request.Files.Count == 0)
            {
                result.Add("Belum ada file yang dipilih");
                return Json(result);
            }

            var currentUser = this.GetCurrentUser();

            foreach (var item in Request.Files)
            {
                try
                {
                    var file = Request.Files[item.ToString()];

                    var fileName = Path.GetFileName(file.FileName);
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.FileName);
                    var systemFileName = FileHelper.CreateSystemFileName(file.FileName);

                    ValidateUploadedDetailFile(file);

                    //save detail document
                    DocumentViewModel document = null;
                    DocumentTypeViewModel docType = null;

                    //default document sjf
                    document = documentLogic.GetByContractNumber(fileNameWithoutExtension);
                    if (document != null) docType = docTypeLogic.GetByName(DEFAULT_DOC_TYPE);


                    //document akta
                    string nomorAkta = "";
                    if (fileNameWithoutExtension.Contains("-"))
                    {
                        var splitedFileName = SplitFileName(fileName);
                        if (splitedFileName.Count() > 1)
                        {
                            var contractNumber = splitedFileName[1];
                            document = documentLogic.GetByContractNumber(contractNumber);
                            if (document != null)
                            {
                                nomorAkta = splitedFileName[0];
                                docType = docTypeLogic.GetByName("AKTA");
                            }
                        }
                    }


                    //other document
                    if (document == null)
                    {
                        var splitedFileName = GetSplitedFileName(fileName);
                        var docNumber = splitedFileName[0];
                        var docTypeName = splitedFileName[1].ToUpper();

                        document = documentLogic.GetByContractNumber(docNumber);
                        if (document == null) document = documentLogic.GetByBillID(docNumber);
                        if (document == null) document = documentLogic.GetByNumber(docNumber);

                        if (document == null)
                            throw new Exception($"File: {fileName} - No. Dokumen/No. Kontrak {docNumber} tidak ditemukan");

                        docType = docTypeLogic.GetByName(docTypeName);
                    }


                    if (docType == null)
                        throw new Exception($"File: {fileName} - Tipe dokumen tidak ditemukan");


                    if (docType.Name == "SJF")
                    {
                        document.Status = Status.Done;
                        document.DoneById = currentUser?.Id;
                        document.DoneDate = DateTime.Now;
                    }

                    documentLogic.Update(document, currentUser);


                    bool isNew = false;
                    var documentDetail = documentDetailLogic.Get(document.Id, docType.Name);
                    if (documentDetail == null)
                    {
                        documentDetail = new DocumentDetailViewModel();
                        isNew = true;
                    }

                    documentDetail.DocumentId = document.Id;
                    documentDetail.DocumentTypeId = docType.Id;
                    documentDetail.OriginalFileName = fileName;
                    documentDetail.SystemFileName = systemFileName;
                    if (isNew) documentDetailLogic.Create(documentDetail, currentUser);
                    else documentDetailLogic.Update(documentDetail, currentUser);
                    //end save document

                    var documentFolder = this.GetDocumentFolder(document.UserDocumentFolder, document.Date.Year);
                    this.SaveUploadedFile(file, documentFolder, systemFileName);

                    db.SaveChanges();

                    result.Add("Success: " + fileName);
                }
                catch (Exception ex)
                {
                    result.Add("Failed:" + ex.Message);
                }
            }

            return Json(result);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public ActionResult UploadExcel()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeAjax("SuperAdmin", "Admin")]
        public JsonResult UploadExcelFile()
        {
            var result = new List<string>();

            try
            {
                if (Request.Files.Count == 0)
                    throw new Exception("Belum ada file yang dipilih.");

                var currentUser = this.GetCurrentUser();

                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var systemFileName = FileHelper.CreateSystemFileName(file.FileName);

                var folderPath = Server.MapPath(EXCEL_IMPORT_PATH);
                this.SaveUploadedFile(file, folderPath, systemFileName);

                var filePath = Path.Combine(folderPath, systemFileName);
                var dataTable = ExcelHelper.ImportToDataTable(filePath);

                int recordNo = 1;
                foreach (var item in dataTable.AsEnumerable())
                {
                    try
                    {
                        var noUrut = item.GetRowCellValue("NO URUT").Stringify();
                        var namaFin = item.GetRowCellValue("NAMA FIN").Stringify();
                        var nomorAkta = item.GetRowCellValue("NOMOR AKTA").Stringify();
                        var tanggalAkta = item.GetRowCellValue("TANGGAL AKTA").ToDateTime();
                        var namaNasabah = item.GetRowCellValue("NAMA NASABAH").Stringify();
                        var nomorPK = item.GetRowCellValue("NOMOR PK").Stringify();
                        var tanggalPK = item.GetRowCellValue("TANGGAL PK").ToDateTime();
                        var tanggalSJF = item.GetRowCellValue("TANGGAL SJF").ToDateTime();
                        var nomorSJF = item.GetRowCellValue("NOMOR SJF").Stringify();
                        var nomorPendaftaran = item.GetRowCellValue("NOMOR PENDAFTARAN").Stringify();
                        var billID = item.GetRowCellValue("BILL ID").Stringify();
                        var notaris = item.GetRowCellValue("NOTARIS").Stringify();

                        var document = documentLogic.GetByContractNumber(nomorPK);
                        if (document == null)
                            throw new Exception($"No. Kontrak: {nomorPK} tidak ditemukan.");

                        document.AktaNumber = nomorAkta;
                        document.AktaDate = tanggalAkta;
                        document.CustomerName = namaNasabah;
                        document.ContractDate = tanggalPK;
                        document.SertifikatDate = tanggalSJF;
                        document.SertifikatNumber = nomorSJF;
                        document.RegistrationNumber = nomorPendaftaran;
                        document.BillID = billID;
                        documentLogic.Update(document, currentUser);
                        db.SaveChanges();

                        result.Add($"Success: #{recordNo}");
                    }
                    catch (Exception ex)
                    {
                        result.Add($"Failed: #{recordNo} - {ex.Message}");
                    }

                    recordNo++;
                }
            }
            catch (Exception ex)
            {
                result.Add($"Error: " + ex.Message);
            }

            return Json(result);
        }

        private void ValidateUploadedDetailFile(HttpPostedFileBase file)
        {
            if (file == null)
                throw new Exception("File kosong.");

            if (file.ContentLength == 0)
                throw new Exception("Belum ada file yang di upload.");

            var fileName = Path.GetFileName(file.FileName);

            if (file.ContentLength > MAX_FILE_SIZE)
                throw new Exception(fileName + " - Ukuran file tidak boleh lebih dari 10 MB.");

            var fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!ALLOWED_FILE_TYPE.Split(' ').Contains(fileExtension))
                throw new Exception(fileName + " - Tipe file yang diupload harus " + ALLOWED_FILE_TYPE);
        }

        private string[] GetSplitedFileName(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName).Split('_');
        }

        private string[] SplitFileName(string fileName, char splitter = '-')
        {
            return Path.GetFileNameWithoutExtension(fileName).Split(splitter);
        }

        [HttpPost]
        [AuthorizeAjax("SuperAdmin")]
        public JsonResult DeleteDocument(Guid? id)
        {
            try
            {
                documentLogic.Delete(id);
                db.SaveChanges();
                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json("Failed: " + ex.Message);
            }
        }

        [HttpGet]
        [AuthorizeUser("SuperAdmin", "Admin")]
        public FileResult DownloadRevision(Guid id)
        {
            var docRevision = documentRevisionLogic.Get(id);
            documentRevisionLogic.Update(docRevision, this.GetCurrentUser());
            db.SaveChanges();

            var path = this.GetDocumentPath
                        (
                            docRevision.UserDocumentFolder,
                            docRevision.DocumentDate.Year,
                            docRevision.SystemFileName
                        );

            if (System.IO.File.Exists(path))
            {
                var extension = Path.GetExtension(docRevision.SystemFileName).Replace(".", "");
                var result = File(path, extension, docRevision.OriginalFileName);
                return result;
            }
            else return this.CreateNotFoundFile(docRevision.OriginalFileName);
        }
    }
}