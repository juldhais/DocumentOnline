﻿using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Logics;
using DocumentOnline.ViewModels;
using System;
using System.Web.Mvc;

namespace DocumentOnline.Controllers
{
    public class AdminManagementController : Controller
    {
        private DataContext db;
        private UserLogic userLogic;

        public AdminManagementController()
        {
            db = new DataContext();
            userLogic = new UserLogic(db);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AuthorizeUser("SuperAdmin", "Admin")]
        public JsonResult GetData(DataTableParameter parameter)
        {
            var result = userLogic.GetListAdminDataTable(this.GetCurrentUser(), parameter);

            return Json(result);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public ActionResult Create()
        {
            return View(new UserViewModel());
        }

        [HttpPost]
        [AuthorizeUser("SuperAdmin", "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var currentUser = this.GetCurrentUser();

                    userViewModel.ParentId = currentUser.Id;
                    userViewModel.Role = Role.Admin;

                    userLogic.Create(userViewModel, currentUser);

                    db.SaveChanges();

                    ViewBag.Message = "Data admin berhasil disimpan.";

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                throw;
            }

            return View(userViewModel);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public ActionResult Update(Guid? id)
        {
            var user = userLogic.Get(id);
            user.Password = EncryptHelper.Decrypt(user.Password);

            return View(user);
        }

        [HttpPost]
        [AuthorizeUser("SuperAdmin", "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var currentUser = Session["CurrentUser"] as UserViewModel;

                    userLogic.Update(userViewModel, currentUser);

                    db.SaveChanges();

                    ViewBag.Message = "Data user berhasil disimpan.";

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                throw;
            }

            return View(userViewModel);
        }

        [AuthorizeUser("SuperAdmin", "Admin")]
        public JsonResult Delete(Guid? id)
        {
            try
            {
                userLogic.Delete(id, this.GetCurrentUser());

                db.SaveChanges();

                return Json("Data user berhasil dihapus.", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}