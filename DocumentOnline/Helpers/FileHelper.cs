﻿using System;
using System.IO;

namespace DocumentOnline.Helpers
{
    public static class FileHelper
    {
        public static string CreateSystemFileName(string originalFileName)
        {
            var guid = Guid.NewGuid().ToString().Left(8);
            var extension = Path.GetExtension(originalFileName);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(originalFileName);
            var systemFileName = fileNameWithoutExtension + "_" + guid + extension;
            return systemFileName;
        }
    }
}