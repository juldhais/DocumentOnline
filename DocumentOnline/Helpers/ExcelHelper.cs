﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace DocumentOnline.Helpers
{
    public static class ExcelHelper
    {
        public static DataSet ImportToDataSet(string filename, bool hasHeaders = true)
        {
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn = "";

            var ext = Path.GetExtension(filename);

            if (ext == ".xls")
            {   //For Excel 97-03
                strConn = $@"Provider=Microsoft.Jet.OLEDB.4.0;
                Data Source = {filename}; Extended Properties = 'Excel 8.0;HDR={HDR};IMEX=1'";
            }
            else if (ext == ".xlsx")
            {    //For Excel 07 and greater
                strConn = $@"Provider=Microsoft.ACE.OLEDB.12.0;
                Data Source = {filename}; Extended Properties = 'Excel 8.0;HDR={HDR};IMEX=1'";
            }
            else throw new Exception("Format file yang diupload harus .xls atau xlsx");

            DataSet output = new DataSet();

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();

                DataTable schemaTable = conn.GetOleDbSchemaTable(
                OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString();

                    OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "]", conn);
                    cmd.CommandType = CommandType.Text;

                    DataTable outputTable = new DataTable(sheet);
                    output.Tables.Add(outputTable);
                    new OleDbDataAdapter(cmd).Fill(outputTable);
                }
            }
            return output;
        }

        public static DataTable ImportToDataTable(string fileName, bool hasheaders = true)
        {
            try
            {
                return ImportToDataSet(fileName).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static object GetRowCellValue(this DataRow dataRow, string columnName)
        {
            try
            {
                var value = dataRow[columnName];
                return value;
            }
            catch
            {
                return null;
            }
        }
    }
}
