﻿using DocumentOnline.ViewModels;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DocumentOnline.Helpers
{
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        private string[] roles;
        private bool isLogin;

        public AuthorizeUserAttribute(params string[] roles)
        {
            this.roles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Session["CurrentUser"] == null)
            {
                isLogin = false;
                return false;
            }

            isLogin = true;

            if (!this.roles.Any()) return true;

            var currentUser = httpContext.Session["CurrentUser"] as UserViewModel;
            return roles.Contains(currentUser.Role);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            RouteValueDictionary route;

            if (isLogin)
                route = new RouteValueDictionary(new { controller = "Home", action = "UnauthorizedUser" });
            else
                route = new RouteValueDictionary(new { controller = "Home", action = "Login" });

            filterContext.Result = new RedirectToRouteResult(route);
        }
    }
}