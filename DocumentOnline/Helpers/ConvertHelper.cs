﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace DocumentOnline.Helpers
{
    public static class ConvertHelper
    {
        //check
        public static bool IsZero(this object value)
        {
            bool result;

            if (value.ToInteger() == 0)
                result = true;
            else
                result = false;

            return result;
        }

        public static bool IsNotZero(this object value)
        {
            bool result;

            if (value.ToInteger() == 0)
                result = false;
            else
                result = true;

            return result;
        }

        public static bool IsEmpty(this object value)
        {
            bool result = true;

            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                result = true;
            else
                result = false;

            return result;
        }

        public static bool IsNotEmpty(this object value)
        {
            return !IsEmpty(value);
        }

        public static bool IsGuidEmpty(this Guid? value)
        {
            if (value == null || value == Guid.Empty) return true;
            return false;
        }

        public static bool IsGuidNotEmpty(this Guid? value)
        {
            return !value.IsGuidEmpty();
        }

        public static bool IsGuidEmpty(this Guid value)
        {
            if (value == Guid.Empty) return true;
            return false;
        }

        public static bool IsGuidNotEmpty(this Guid value)
        {
            return !value.IsGuidEmpty();
        }

        //convert
        public static decimal ToDecimal(this object value)
        {
            decimal result;

            try
            {
                result = Convert.ToDecimal(value);
            }
            catch
            {
                result = 0;
            }

            return result;
        }

        public static decimal ToRoundedDecimal(this object value)
        {
            decimal result;

            try
            {
                result = decimal.Round(Convert.ToDecimal(value), 0);
            }
            catch
            {
                result = 0;
            }

            return result;
        }

        public static decimal? ToNullableDecimal(this object value)
        {
            decimal? result;

            try
            {
                if (value.IsEmpty()) return null;
                else result = Convert.ToDecimal(value);
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public static int ToInteger(this object value)
        {
            int result;

            try
            {
                result = Convert.ToInt32(value);
            }
            catch
            {
                result = 0;
            }

            return result;
        }

        public static int? ToNullableInteger(this object value)
        {
            int? result;

            try
            {
                if (value.IsEmpty()) return null;
                else result = Convert.ToInt32(value);
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public static DateTime ToDateTime(this object value)
        {
            try
            {
                if (value.IsEmpty()) return new DateTime();
                else return Convert.ToDateTime(value);
            }
            catch
            {
                return new DateTime();
            }
        }

        public static DateTime? ToNullableDateTime(this object value)
        {
            try
            {
                if (value.IsEmpty()) return null;
                else return Convert.ToDateTime(value);
            }
            catch
            {
                return null;
            }
        }

        public static string Stringify(this object value)
        {
            string result;

            try
            {
                if (value.IsEmpty()) result = string.Empty;
                else result = value.ToString();
            }
            catch
            {
                result = string.Empty;
            }


            return result;
        }

        public static Guid? ToGuid(this object value)
        {
            try
            {
                return Guid.Parse(value.Stringify());
            }
            catch
            {
                return null;
            }
        }

        public static bool ToBoolean(this object value)
        {
            try
            {
                var stringValue = value.Stringify();
                if (stringValue.ToUpper() == "TRUE" || stringValue == "1")
                    return true;
                else if (stringValue.ToUpper() == "FALSE" || stringValue == "0")
                    return false;
                else return Convert.ToBoolean(value);
                
            }
            catch
            {
                return false;
            }
        }

        public static string ToShortDate(this DateTime datetime)
        {
            return datetime.ToString("dd/MM/yyyy");
        }

        public static object TypeConvert(this object value, Type type)
        {
            object result = value;
            if (type == typeof(decimal)) result = value.ToDecimal();
            else if (type == typeof(decimal?)) result = value.ToNullableDecimal();
            else if (type == typeof(string)) result = value.Stringify();
            else if (type == typeof(int)) result = value.ToInteger();
            else if (type == typeof(int?)) result = value.ToNullableInteger();
            else if (type == typeof(Guid)) result = value.ToGuid().Value;
            else if (type == typeof(Guid?)) result = value.ToGuid();
            else if (type == typeof(DateTime)) result = value.ToDateTime();
            else if (type == typeof(DateTime?)) result = value.ToNullableDateTime();

            return result;
        }

        public static decimal RoundUp(this decimal value, int rounding = 1)
        {
            return value + rounding - (value % rounding);
        }

        public static decimal RoundDown(this decimal value, int rounding = 1)
        {
            return value - (value % rounding);
        }

        public static decimal RoundBest(this decimal value, int rounding = 1, decimal tolerance = 50)
        {
            var roundedDown = value.RoundDown(rounding);
            var diff = value - roundedDown;
            var diffPercentage = diff / rounding * 100;
            if (diffPercentage > tolerance) return RoundUp(value, rounding);
            else return RoundDown(value, rounding);
        }

        public static decimal MultiplyBy(this decimal value, decimal multiplier)
        {
            return value * multiplier;
        }

        public static decimal DivideBy(this decimal value, decimal divider)
        {
            if (divider == 0) return value;
            else return value / divider;
        }

        //num
        public static string Num(this decimal value)
        {
            return value.ToString("#,#0.####");
        }

        public static string Num(this int value)
        {
            return value.ToString("#,#0.####");
        }

        public static string Num0(this decimal value)
        {
            return value.ToString("#,#0");
        }

        public static string Num2(this decimal value)
        {
            return value.ToString("#,#0.##");
        }


        //text
        public static string SplitCase(this string input)
        {
            return Regex.Replace(input, "([A-Z])", " $1", RegexOptions.Compiled).Trim();
        }

        public static string Loop(this string text, int length)
        {
            string result = string.Empty;

            for (int i = 0; i < length; i++)
            {
                result = result + text;
            }

            return result;
        }

        public static string Line(int length)
        {
            return "-".Loop(length);
        }

        public static string Line2(int length)
        {
            return "=".Loop(length);
        }

        public static string Left(this string text, int length)
        {
            string result = string.Empty;

            if (text.Length == length)
                result = text;
            else if (text.Length > length)
                result = text.Substring(0, length);
            else if (text.Length < length)
                result = text.PadRight(length);

            return result;
        }

        public static string Right(this string text, int length)
        {
            string result = string.Empty;

            if (text.Length == length)
                result = text;
            else if (text.Length > length)
                result = text.Substring(text.Length - length, length);
            else if (text.Length < length)
                result = text.PadLeft(length);

            return result;
        }

        public static string Center(this string text, int length)
        {
            string result = string.Empty;

            if (text.Length == length)
                result = text;
            else if (text.Length > length)
                result = text.Substring(0, length);
            else if (text.Length < length)
            {
                int space = (length - text.Length) / 2;
                for (int i = 0; i < space; i++)
                    result += " ";
                result += text;
            }

            return result;
        }


        //rnum decimal
        public static string RNum2(this decimal value, int length)
        {
            return value.Num2().Right(length);
        }

        public static string RNum0(this decimal value, int length)
        {
            return value.Num0().Right(length);
        }

        public static string RNum(this decimal value, int length)
        {
            return value.Num().Right(length);
        }

        public static string RNum2(this decimal? value, int length)
        {
            if (value.HasValue)
                return value.Value.Num2().Right(length);
            else return "0,00".Right(length);
        }

        public static string RNum0(this decimal? value, int length)
        {
            if (value.HasValue)
                return value.Value.Num0().Right(length);
            else return "0".Right(length);
        }

        public static string RNum(this decimal? value, int length)
        {
            if (value.HasValue)
                return value.Value.Num().Right(length);
            else return " ".Right(length);
        }

        //terbilang
        public static string Terbilang(int x)
        {
            string[] bilangan = { "", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS" };
            string temp = string.Empty;

            if (x < 12)
            {
                temp = " " + bilangan[x];
            }
            else if (x < 20)
            {
                temp = Terbilang(x - 10) + " BELAS";
            }
            else if (x < 100)
            {
                temp = Terbilang(x / 10) + " PULUH" + Terbilang(x % 10);
            }
            else if (x < 200)
            {
                temp = " SERATUS" + Terbilang(x - 100);
            }
            else if (x < 1000)
            {
                temp = Terbilang(x / 100) + " RATUS" + Terbilang(x % 100);
            }
            else if (x < 2000)
            {
                temp = " SERIBU" + Terbilang(x - 1000);
            }
            else if (x < 1000000)
            {
                temp = Terbilang(x / 1000) + " RIBU" + Terbilang(x % 1000);
            }
            else if (x < 1000000000)
            {
                temp = Terbilang(x / 1000000) + " JUTA" + Terbilang(x % 1000000);
            }

            return temp;
        }

        public static string Terbilang(this decimal value)
        {
            return Terbilang(value.ToInteger()).Replace("  ", " ").Trim();
        }

        public static string TerbilangRupiah(this decimal value)
        {
            return (Terbilang((value.ToInteger())) + " RUPIAH").Replace("  ", " ").Trim();
        }

        //image
        public static System.Drawing.Imaging.ImageFormat GetImageFormat(this Image img)
        {
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg))
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp))
                return System.Drawing.Imaging.ImageFormat.Bmp;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png))
                return System.Drawing.Imaging.ImageFormat.Png;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Emf))
                return System.Drawing.Imaging.ImageFormat.Emf;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Exif))
                return System.Drawing.Imaging.ImageFormat.Exif;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif))
                return System.Drawing.Imaging.ImageFormat.Gif;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Icon))
                return System.Drawing.Imaging.ImageFormat.Icon;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.MemoryBmp))
                return System.Drawing.Imaging.ImageFormat.MemoryBmp;
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Tiff))
                return System.Drawing.Imaging.ImageFormat.Tiff;
            else
                return System.Drawing.Imaging.ImageFormat.Wmf;
        }

        public static string GetImageExtension(this System.Drawing.Image img)
        {
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg))
                return "jpg";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp))
                return "bmp";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png))
                return "png";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Emf))
                return "emf";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Exif))
                return "exif";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif))
                return "gif";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Icon))
                return "ico";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.MemoryBmp))
                return "bmp";
            if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Tiff))
                return "tiff";
            else
                return "wmf";
        }

        public static byte[] ImageToByteArray(this Image image)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                image.Save(ms, image.GetImageFormat());
                return ms.ToArray();
            }
            catch
            {
                return null;
            }

        }

        public static Image ByteArrayToImage(byte[] bytearray)
        {
            try
            {
                MemoryStream ms = new MemoryStream(bytearray);
                Image image = Image.FromStream(ms);
                return image;
            }
            catch
            {
                return null;
            }

        }

        public static void CompressAndSaveImage(Image image, string filename, long quality)
        {
            try
            {

                EncoderParameters parameters = new EncoderParameters(1);
                parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                image.Save(filename, GetCodecInfo("image/jpeg"), parameters);
            }
            catch { }
        }

        public static Image CompressImage(this Image image, long quality)
        {
            try
            {
                EncoderParameters parameters = new EncoderParameters(1);
                parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                MemoryStream stream = new MemoryStream();
                image.Save(stream, GetCodecInfo("image/jpeg"), parameters);
                return Image.FromStream(stream);
            }
            catch
            {
                return null;
            }
        }

        public static byte[] CompressImageToByteArray(this Image image, long quality)
        {
            try
            {
                EncoderParameters parameters = new EncoderParameters(1);
                parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                MemoryStream stream = new MemoryStream();
                image.Save(stream, GetCodecInfo("image/jpeg"), parameters);
                return stream.ToArray();
            }
            catch
            {
                return null;
            }
        }

        public static ImageCodecInfo GetCodecInfo(string mimeType)
        {
            foreach (ImageCodecInfo encoder in ImageCodecInfo.GetImageEncoders())
                if (encoder.MimeType == mimeType)
                    return encoder;
            throw new ArgumentOutOfRangeException(
                string.Format("'{0}' not supported", mimeType));
        }


        //other
        public static bool IsNumeric(this Type type)
        {
            if (type == typeof(decimal)) return true;
            else if (type == typeof(int)) return true;
            else if (type == typeof(decimal?)) return true;
            else if (type == typeof(int?)) return true;
            else return false;
        }

        public static int CalculateProgressPercentage(int current, int count, DateTime start, out string timeRemaining)
        {
            double kecepatan = current / (DateTime.Now - start).TotalSeconds;
            double sisawaktu = (count - current) / kecepatan;
            int sisajam = (sisawaktu / 3600).ToInteger();
            int sisamenit = ((sisawaktu % 3600) / 60).ToInteger();
            int sisadetik = ((sisawaktu % 3600) % 60).ToInteger();

            timeRemaining = string.Empty;
            if (sisajam != 0) timeRemaining = sisajam + " jam ";
            if (sisamenit != 0) timeRemaining += sisamenit + " menit ";
            if (sisadetik != 0) timeRemaining += sisadetik + " detik ";
            if (timeRemaining.IsNotEmpty()) timeRemaining = "[" + timeRemaining + "lagi...]";

            return (current * 100 / count).ToInteger();
        }

        public static string GetWeekInMonthAndYear(this DateTime date)
        {
            DateTime tempdate = date.AddDays(-date.Day + 1);

            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNumStart = ciCurr.Calendar.GetWeekOfYear(tempdate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            int weekNum = ciCurr.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            int weekInMonth = weekNum - weekNumStart + 1;
            return "M" + weekInMonth + "/" + date.Month + "/" + date.Year;
        }

        //stream
        public static Stream ConvertToStream(this string text)
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(text);
                writer.Flush();
                stream.Position = 0;
                stream.Seek(0, SeekOrigin.Begin);

                return stream;
            }
            catch
            {
                return null;
            }
        }

        public static string ConvertToString(this Stream stream)
        {
            try
            {
                StreamReader reader = new StreamReader(stream);
                string text = reader.ReadToEnd();

                return text;
            }
            catch
            {
                return "";
            }
        }

        public static string RemoveSpecialCharacters(this string str)
        {
            if (str.IsEmpty()) return "";

            var sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') 
                    || (c >= 'A' && c <= 'Z') 
                    || (c >= 'a' && c <= 'z') 
                    || c == '.' 
                    || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}
