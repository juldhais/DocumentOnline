﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;

namespace DocumentOnline.Mapping
{
    public class InvoiceProfile : Profile
    {
        public InvoiceProfile()
        {
            CreateMap<Invoice, InvoiceViewModel>();
            CreateMap<InvoiceViewModel, Invoice>();
        }
    }
}