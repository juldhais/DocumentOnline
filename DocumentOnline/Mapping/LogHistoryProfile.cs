﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;

namespace DocumentOnline.Mapping
{
    public class LogHistoryProfile : Profile
    {
        public LogHistoryProfile()
        {
            CreateMap<LogHistory, LogHistoryViewModel>();
            CreateMap<LogHistoryViewModel, LogHistory>();

        }
    }
}