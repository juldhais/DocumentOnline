﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;

namespace DocumentOnline.Mapping
{
    public class DocumentRecapProfile : Profile
    {
        public DocumentRecapProfile()
        {
            CreateMap<DocumentRecap, DocumentRecapViewModel>();
            CreateMap<DocumentRecapViewModel, DocumentRecap>();
        }
    }
}