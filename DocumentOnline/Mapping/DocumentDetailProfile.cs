﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;

namespace DocumentOnline.Mapping
{
    public class DocumentDetailProfile : Profile
    {
        public DocumentDetailProfile()
        {
            CreateMap<DocumentDetail, DocumentDetailViewModel>()
                .ForMember(dest => dest.UserDocumentFolder,
                    opt => opt.MapFrom(src => src.Document.User.DocumentFolder));

            CreateMap<DocumentDetailViewModel, DocumentDetail>();

        }
    }
}