﻿using AutoMapper;

namespace DocumentOnline.Mapping
{
    public static class MappingConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<UserProfile>();
                x.AddProfile<NotificationProfile>();
                x.AddProfile<DocumentTypeProfile>();
                x.AddProfile<DocumentProfile>();
                x.AddProfile<DocumentDetailProfile>();
                x.AddProfile<InvoiceProfile>();
                x.AddProfile<DocumentRecapProfile>();
                x.AddProfile<DocumentRevisionProfile>();
                x.AddProfile<LogHistoryProfile>();
            });
        }
    }
}