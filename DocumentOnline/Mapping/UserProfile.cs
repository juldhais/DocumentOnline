﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;

namespace DocumentOnline.Mapping
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<UserViewModel, User>();
        }
    }
}