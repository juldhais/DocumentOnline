﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;

namespace DocumentOnline.Mapping
{
    public class DocumentTypeProfile : Profile
    {
        public DocumentTypeProfile()
        {
            CreateMap<DocumentType, DocumentTypeViewModel>();
            CreateMap<DocumentTypeViewModel, DocumentType>();
        }
    }
}