﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;

namespace DocumentOnline.Mapping
{
    public class DocumentRevisionProfile : Profile
    {
        public DocumentRevisionProfile()
        {
            CreateMap<DocumentRevision, DocumentRevisionViewModel>()
                .ForMember(dest => dest.UserDocumentFolder,
                    opt => opt.MapFrom(src => src.Document.User.DocumentFolder));

            CreateMap<DocumentRevisionViewModel, DocumentRevision>();

        }
    }
}