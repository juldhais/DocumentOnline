﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DocumentOnline.Logics
{
    public class DocumentLogic
    {
        private DataContext db;

        public DocumentLogic(DataContext db)
        {
            this.db = db;
        }

        public DocumentViewModel Get(Guid? id)
        {
            var document = db.Documents.Where(x => x.Id == id)
                            .ProjectTo<DocumentViewModel>()
                            .FirstOrDefault();

            return document;
        }

        public DocumentViewModel GetByNumber(string number)
        {
            var document = db.Documents.Where(x => x.Number == number)
                            .ProjectTo<DocumentViewModel>()
                            .FirstOrDefault();

            return document;
        }

        public DocumentViewModel GetByContractNumber(string contractNumber)
        {
            var document = db.Documents.Where(x => x.ContractNumber == contractNumber)
                            .ProjectTo<DocumentViewModel>()
                            .FirstOrDefault();

            return document;
        }

        public DocumentViewModel GetByBillID(string billID)
        {
            var document = db.Documents.Where(x => x.BillID == billID)
                            .ProjectTo<DocumentViewModel>()
                            .FirstOrDefault();

            return document;
        }

        public DataTableResult GetListDataTable(Guid? userId, DataTableParameter parameter)
        {
            var searchBy = (parameter.search != null) ? parameter.search.value : null;
            var take = parameter.length;
            var skip = parameter.start;

            //TODO : implement order

            var query = db.Documents.AsQueryable();

            //filter user
            var user = db.Users.Find(userId);
            if (user.Role == Role.SuperUser)
            {
                var listUserId = db.Users.Where(x => x.ParentId == userId)
                                    .Select(x => (Guid?)x.Id).ToList();

                listUserId.Add(userId);

                query = query.Where(x => listUserId.Contains(x.UserId));
            }
            else if (user.Role == Role.User)
                query = query.Where(x => x.UserId == userId);
            //end filter user

            var totalResult = query.Count();

            //custom filter
            if (parameter.dateColumn.IsNotEmpty() && parameter.fromDate.HasValue && parameter.toDate.HasValue)
            {
                var fromDate = parameter.fromDate.Value.Date;
                var toDate = parameter.toDate.Value.Date;

                if (parameter.dateColumn == "Date")
                    query = query.Where(x => DbFunctions.TruncateTime(x.Date) >= fromDate
                                          && DbFunctions.TruncateTime(x.Date) <= toDate);
                else if (parameter.dateColumn == "ContractDate")
                    query = query.Where(x => DbFunctions.TruncateTime(x.ContractDate) >= fromDate
                      && DbFunctions.TruncateTime(x.ContractDate) <= toDate);
            }

            var filterStatus = "";
            if (parameter.filter != null)
            {
                foreach (var item in parameter.filter)
                {
                    if (item.keyword.IsEmpty() || item.column.IsEmpty()) continue;

                    if (item.column == "CompanyName")
                        query = query.Where(x => x.User.CompanyName.Contains(item.keyword));
                    else if (item.column == "BranchName")
                        query = query.Where(x => x.User.BranchName.Contains(item.keyword));
                    else if (item.column == "Status")
                        filterStatus = item.keyword;
                }
            }
            //end custom filter

            //search
            if (searchBy.IsNotEmpty())
                query = query.Where(x => x.Number.StartsWith(searchBy)
                        || x.CustomerName.Contains(searchBy)
                        || x.ContractNumber.StartsWith(searchBy)
                        || x.User.Name.Contains(searchBy)
                        || x.Status.StartsWith(searchBy)
                        || x.User.CompanyName.Contains(searchBy)
                        || x.User.BranchName.Contains(searchBy)
                        || x.OriginalFileName.Contains(searchBy));
            //end search

            var totalOpen = query.Count(x => x.Status == Status.Open || x.Status == Status.ReOpen);
            var totalInProgress = query.Count(x => x.Status == Status.InProgress);
            var totalDone = query.Count(x => x.Status == Status.Done);

            if (filterStatus.IsNotEmpty())
                query = query.Where(x => x.Status == filterStatus);

            var totalFiltered = query.Count();

            var list = query.OrderByDescending(x => x.Number)
                       .Skip(skip)
                       .Take(take)
                       .ProjectTo<DocumentViewModel>()
                       .ToList();

            //unbound
            foreach (var item in list)
            {
                item.StringDate = item.Date.ToShortDate();
                if (item.AktaDate.HasValue) item.StringAktaDate = item.AktaDate.Value.ToShortDate();
                if (item.SertifikatDate.HasValue) item.StringSertifikatDate = item.SertifikatDate.Value.ToShortDate();
                if (item.ContractDate.HasValue) item.StringContractDate = item.ContractDate.Value.ToShortDate();

                item.DetailList = "";
                var docDetailType = db.DocumentDetails.Where(x => x.DocumentId == item.Id).Select(x => x.DocumentType.Name).ToList();
                foreach (var docType in docDetailType)
                    item.DetailList += docType + ",";

                item.RevisionList = new List<IdNameViewModel>();
                var revisionDetail = db.DocumentRevisions.Where(x => x.DocumentId == item.Id).ToList();
                foreach (var revision in revisionDetail)
                {
                    item.RevisionList.Add(new IdNameViewModel
                    {
                        Id = revision.Id,
                        Name = revision.Remarks
                    });
                }
            }

            var extra = new
            {
                totalOpen,
                totalInProgress,
                totalDone,
                totalAll = totalOpen + totalInProgress + totalDone
            };

            var result = new DataTableResult();
            result.draw = parameter.draw;
            result.recordsTotal = totalResult;
            result.recordsFiltered = totalFiltered;
            result.data = list;
            result.extra = extra;
            
            return result;
        }

        public void Create(DocumentViewModel documentViewModel, UserViewModel currentUser)
        {
            var document = Mapper.Map<DocumentViewModel, Document>(documentViewModel);

            document.Id = Guid.NewGuid();
            document.OpenDate = DateTime.Now;
            document.OpenById = currentUser.Id;
            document.DateCreated = DateTime.Now;
            document.CreatedById = currentUser.Id;

            db.Documents.Add(document);
            
            Mapper.Map(document, documentViewModel);
        }

        public void Update(DocumentViewModel documentViewModel, UserViewModel currentUser)
        {
            var document = db.Documents.Find(documentViewModel.Id);

            if (document == null) throw new Exception("Dokumen tidak ditemukan.");

            Mapper.Map(documentViewModel, document);

            document.DateModified = DateTime.Now;
            document.ModifiedById = currentUser.Id;

            documentViewModel.DateModified = document.DateModified;
            document.ModifiedById = currentUser.Id;
        }

        public string CreateDocumentNumber()
        {
            var year = DateTime.Now.Year;
            var month = DateTime.Now.Month;
            var prefix = year.ToString().Substring(2, 2) + month.ToString("00");

            var lastNumber = db.Documents.Where(x => x.Number.StartsWith(prefix))
                             .OrderByDescending(x => x.Number)
                             .Select(x => x.Number)
                             .FirstOrDefault();

            if (lastNumber == null) return prefix + "000001";

            var lastSeries = lastNumber.Substring(prefix.Length, 6);
            var newSeries = lastSeries.ToInteger() + 1;
            var newNumber = prefix + newSeries.ToString("000000");

            return newNumber;
        }

        public void Delete(Guid? id)
        {
            var document = db.Documents.Find(id);

            if (document == null)
                throw new Exception("Dokumen tidak ditemukan.");

            if (db.DocumentDetails.Any(x => x.DocumentId == id))
                throw new Exception($"Dokumen No: {document.ContractNumber} tidak dapat dihapus karena sudah ada detail dokumen.");

            db.Documents.Remove(document);
        }
    }
}