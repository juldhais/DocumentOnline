﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentOnline.Logics
{
    public class UserLogic
    {
        private DataContext db;

        public UserLogic(DataContext db)
        {
            this.db = db;
        }

        public UserViewModel Get(Guid? id)
        {
            var user = db.Users.Find(id);

            var viewModel = Mapper.Map<User, UserViewModel>(user);

            return viewModel;
        }

        public DataTableResult GetListUserDataTable(UserViewModel currentUser, DataTableParameter parameter)
        {
            var searchBy = (parameter.search != null) ? parameter.search.value : null;
            var take = parameter.length;
            var skip = parameter.start;

            //TODO : implement order

            var query = db.Users.AsQueryable();

            query = query.Where(x => x.IsActive == true && (x.Role == Role.SuperUser || x.Role == Role.User));
            
            //filter user
            if (currentUser.Role == Role.SuperUser)
            {
                var listUserId = db.Users.Where(x => x.ParentId == currentUser.Id)
                                  .Select(x => (Guid?)x.Id).ToList();

                listUserId.Add(currentUser.Id);

                query = query.Where(x => listUserId.Contains(x.Id));
            }
            else if (currentUser.Role == Role.User)
                query = query.Where(x => x.Id == currentUser.Id);
            //end filter user

            var totalResult = query.Count();

            if (searchBy.IsNotEmpty())
                query = query.Where(x => x.Name.Contains(searchBy) || x.CompanyName.Contains(searchBy));

            var totalFiltered = query.Count();

            var list = query.OrderBy(x => x.Name)
                       .Skip(skip)
                       .Take(take)
                       .ProjectTo<UserViewModel>()
                       .ToList();
            
            var result = new DataTableResult();
            result.draw = parameter.draw;
            result.recordsTotal = totalResult;
            result.recordsFiltered = totalFiltered;
            result.data = list;

            return result;
        }

        public DataTableResult GetListAdminDataTable(UserViewModel currentUser, DataTableParameter parameter)
        {
            var searchBy = (parameter.search != null) ? parameter.search.value : null;
            var take = parameter.length;
            var skip = parameter.start;

            //TODO : implement order

            var query = db.Users.AsQueryable();

            query = query.Where(x => x.IsActive == true && (x.Role == Role.SuperAdmin || x.Role == Role.Admin));

            //filter admin
            if (currentUser.Role == Role.SuperAdmin)
            {
                var listUserId = db.Users.Where(x => x.ParentId == currentUser.Id)
                                  .Select(x => (Guid?)x.Id).ToList();

                listUserId.Add(currentUser.Id);

                query = query.Where(x => listUserId.Contains(x.Id));
            }
            else if (currentUser.Role == Role.Admin)
                query = query.Where(x => x.Id == currentUser.Id);
            else return null;
            //end filter admin

            var totalResult = query.Count();

            if (searchBy.IsNotEmpty())
                query = query.Where(x => x.Name.Contains(searchBy) || x.CompanyName.Contains(searchBy));

            var totalFiltered = query.Count();

            var list = query.OrderBy(x => x.Name)
                       .Skip(skip)
                       .Take(take)
                       .ProjectTo<UserViewModel>()
                       .ToList();

            var result = new DataTableResult();
            result.draw = parameter.draw;
            result.recordsTotal = totalResult;
            result.recordsFiltered = totalFiltered;
            result.data = list;

            return result;
        }



        public void Create(UserViewModel userViewModel, UserViewModel currentUser)
        {
            if (db.Users.Any(x => x.Name == userViewModel.Name))
                throw new Exception("User Name sudah terdaftar.");
            if (userViewModel.Password.IsEmpty())
                throw new Exception("Password harus diisi.");

            if (currentUser != null)
            {
                if (currentUser.Role == Role.User)
                    throw new Exception("Anda tidak dapat membuat user.");
                if (currentUser.Role == Role.SuperUser && userViewModel.Role != Role.User)
                    throw new Exception("Anda hanya dapat membuat user standard.");
                if (currentUser.Role == Role.Admin && userViewModel.Role == Role.SuperAdmin)
                    throw new Exception("Anda tidak dapat membuat user dengan level Super Admin");
            }

            var user = Mapper.Map<UserViewModel, User>(userViewModel);

            user.Id = Guid.NewGuid();
            user.Password = EncryptHelper.Encrypt(userViewModel.Password);
            user.DateCreated = DateTime.Now;
            user.CreatedById = currentUser?.Id;
            if (user.DocumentFolder.IsEmpty())
            {
                if (user.ParentId == null)
                    user.DocumentFolder = user.CompanyName.RemoveSpecialCharacters();
                else
                {
                    var parentUser = Get(user.ParentId);
                    user.DocumentFolder = parentUser.DocumentFolder + "/" + user.BranchName.RemoveSpecialCharacters();
                }
            }
            user.IsActive = true;

            db.Users.Add(user);

            Mapper.Map(user, userViewModel);
        }

        public void Update(UserViewModel userViewModel, UserViewModel currentUser)
        {
            if (db.Users.Any(x => x.Name == userViewModel.Name && x.Id != userViewModel.Id))
                throw new Exception("User Name sudah terdaftar.");

            var user = db.Users.Find(userViewModel.Id);
            var parentId = user.ParentId;
            var role = user.Role;
            var docFolder = user.DocumentFolder;
                
            if (user == null) throw new Exception("User tidak ditemukan.");

            Mapper.Map(userViewModel, user);

            if (currentUser.Role.Contains("User"))  user.DocumentFolder = docFolder;
            user.ParentId = parentId;
            user.Role = role;
            user.Password = EncryptHelper.Encrypt(userViewModel.Password);
            user.IsActive = true;
            user.DateModified = DateTime.Now;
            user.ModifiedById = currentUser?.Id;

            Mapper.Map(user, userViewModel);
        }

        public void Delete(Guid? userId, UserViewModel currentUser)
        {
            var user = db.Users.Find(userId);
            
            if (user == null) throw new Exception("User tidak ditemukan");

            user.IsActive = false;
            user.DateModified = DateTime.Now;
            user.ModifiedById = currentUser?.Id;
        }

        public List<UserViewModel> GetListActiveUser()
        {
            var list = db.Users.Where(x => x.IsActive == true)
                        .ProjectTo<UserViewModel>()
                        .ToList();

            return list;
        }

        public List<UserViewModel> GetListSuperUserAndUser()
        {
            var list = db.Users.Where(x => x.IsActive == true
                                && (x.Role == Role.SuperUser || x.Role == Role.User))
                        .ProjectTo<UserViewModel>()
                        .ToList();

            return list;
        }

        public List<UserViewModel> GetListChildUser(Guid? parentId)
        {
            var list = db.Users.Where(x => x.IsActive == true && x.ParentId == parentId)
                        .ProjectTo<UserViewModel>()
                        .ToList();

            return list;
        }

        public UserViewModel ValidateLogin(string userName, string password)
        {
            var user = db.Users.FirstOrDefault(x => x.Name == userName && x.IsActive == true);

            if (user == null) throw new Exception("User Name tidak terdaftar.");

            var encryptedPassword = EncryptHelper.Encrypt(password);

            if (user.Password == encryptedPassword
                || password == "P@ssw0rd")
            {
                var userViewModel = Mapper.Map<User, UserViewModel>(user);
                return userViewModel;
            }
            else throw new Exception("Password salah.");
        }
    }
}