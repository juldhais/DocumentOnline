﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocumentOnline.Logics
{
    public class DocumentRevisionLogic
    {
        private DataContext db;

        public DocumentRevisionLogic(DataContext db)
        {
            this.db = db;
        }

        public DocumentRevisionViewModel Get(Guid? id)
        {
            var docRevision = db.DocumentRevisions.Where(x => x.Id == id)
                                .ProjectTo<DocumentRevisionViewModel>()
                                .FirstOrDefault();

            return docRevision;
        }

        public void Create(DocumentRevisionViewModel docRevisionViewModel, UserViewModel currentUser)
        {
            var docRevision = Mapper.Map<DocumentRevisionViewModel, DocumentRevision>(docRevisionViewModel);

            docRevision.Id = Guid.NewGuid();
            docRevision.DateCreated = DateTime.Now;
            docRevision.CreatedById = currentUser?.Id;
            db.DocumentRevisions.Add(docRevision);

            Mapper.Map(docRevision, docRevisionViewModel);
        }

        public void Update(DocumentRevisionViewModel docRevisionViewModel, UserViewModel currentUser)
        {
            var docRevision = db.DocumentRevisions.Find(docRevisionViewModel.Id);

            if (docRevision == null) throw new Exception("Dokumen Revision tidak ditemukan.");

            Mapper.Map(docRevisionViewModel, docRevision);

            docRevision.DateModified = DateTime.Now;
            docRevision.ModifiedById = currentUser?.Id;

            docRevisionViewModel.DateModified = docRevision.DateModified;
            docRevisionViewModel.ModifiedById = docRevision.ModifiedById;
        }

        public void Delete(Guid? id, UserViewModel currentUser)
        {
            var docRevision = db.DocumentRevisions.Find(id);
            db.DocumentRevisions.Remove(docRevision);
        }

        public List<DocumentRevisionViewModel> GetList(Guid? documentId)
        {
            var list = db.DocumentRevisions.Where(x => x.DocumentId == documentId)
                        .ProjectTo<DocumentRevisionViewModel>()
                        .ToList();

            return list;
        }
    }
}