﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using System.Data.Entity;

namespace DocumentOnline.Logics
{
    public class LogHistoryLogic
    {
        private DataContext db;

        public LogHistoryLogic(DataContext db)
        {
            this.db = db;
        }

        public LogHistoryViewModel Get(Guid? id)
        {
            var log = db.LogHistories.Find(id);

            var logViewModel = Mapper.Map<LogHistory, LogHistoryViewModel>(log);

            return logViewModel;
        }

        public LogHistoryViewModel GetLastLogin(Guid? userId)
        {
            var log = db.LogHistories
                    .Where(x => x.UserId == userId && x.LogoutDate == null)
                    .OrderByDescending(x => x.LoginDate)
                    .ProjectTo<LogHistoryViewModel>()
                    .FirstOrDefault();

            return log;
        }

        public List<LogHistoryViewModel> GetList()
        {
            var list = db.LogHistories.ProjectTo<LogHistoryViewModel>().ToList();
            return list;
        }

        public List<LogHistoryViewModel> GetList(DateTime? date)
        {
            var list = db.LogHistories
                        .Where(x => DbFunctions.TruncateTime(x.LoginDate) == date)
                        .ProjectTo<LogHistoryViewModel>()
                        .ToList();

            return list;
        }

        public void Create(LogHistoryViewModel logViewModel)
        {
            var log = Mapper.Map<LogHistory>(logViewModel);

            log.Id = Guid.NewGuid();

            db.LogHistories.Add(log);

            Mapper.Map(log, logViewModel);
        }

        public void Update(LogHistoryViewModel logViewModel)
        {
            var log = db.LogHistories.Find(logViewModel.Id);

            if (log == null) return;

            Mapper.Map(logViewModel, log);
        }
    }
}