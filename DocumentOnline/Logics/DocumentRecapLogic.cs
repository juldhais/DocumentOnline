﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentOnline.Logics
{
    public class DocumentRecapLogic
    {
        private DataContext db;

        public DocumentRecapLogic(DataContext db)
        {
            this.db = db;
        }

        public DocumentRecapViewModel Get(Guid id)
        {
            var docRecap = db.DocumentRecaps.Where(x => x.Id == id)
                            .ProjectTo<DocumentRecapViewModel>()
                            .FirstOrDefault();

            return docRecap;
        }


        public DataTableResult GetListDataTable(Guid? userId, DataTableParameter parameter)
        {
            var searchBy = (parameter.search != null) ? parameter.search.value : null;
            var take = parameter.length;
            var skip = parameter.start;

            //TODO : implement order

            var query = db.DocumentRecaps.AsQueryable();

            //filter user
            var user = db.Users.Find(userId);
            if (user.Role == Role.SuperUser)
            {
                var listUserId = db.Users.Where(x => x.ParentId == userId)
                                    .Select(x => (Guid?)x.Id).ToList();

                listUserId.Add(userId);

                query = query.Where(x => listUserId.Contains(x.FromUserId) || listUserId.Contains(x.ToUserId));
            }
            else if (user.Role == Role.User)
                query = query.Where(x => x.FromUserId == userId || x.ToUserId == userId);
            //end filter user

            var totalResult = query.Count();

            //custom filter
            if (parameter.dateColumn.IsNotEmpty())
            {
                if (parameter.dateColumn == "Date")
                    query = query.Where(x => DbFunctions.TruncateTime(x.Date) >= parameter.fromDate
                                          && DbFunctions.TruncateTime(x.Date) <= parameter.toDate);
            }

            if (parameter.filter != null)
            {
                foreach (var item in parameter.filter)
                {
                    if (item.keyword.IsEmpty() || item.column.IsEmpty()) continue;

                    if (item.column == "CompanyName")
                        query = query.Where(x => x.ToUser.CompanyName.Contains(item.keyword));
                    else if (item.column == "BranchName")
                        query = query.Where(x => x.ToUser.BranchName.Contains(item.keyword));
                }
            }
            //end custom filter

            if (searchBy.IsNotEmpty())
                query = query.Where(x => x.OriginalFileName.Contains(searchBy));

            var totalFiltered = query.Count();
            var totalDownloaded = query.Count(x => x.DownloadCount > 0);
            var totalNotDownloaded = query.Count(x => x.DownloadCount == 0);

            var list = query.OrderByDescending(x => x.Date)
                       .Skip(skip)
                       .Take(take)
                       .ProjectTo<DocumentRecapViewModel>()
                       .ToList();

            //format date
            Parallel.ForEach(list, item =>
            {
                item.StringDate = item.Date.ToString("dd/MM/yyyy");
                if (item.ToUserId == null)
                {
                    item.ToUserName = "admin";
                    item.ToUserCompanyName = item.FromUserCompanyName;
                    item.ToUserBranchName = item.FromUserBranchName;
                }
            });

            var result = new DataTableResult();
            result.draw = parameter.draw;
            result.recordsTotal = totalResult;
            result.recordsFiltered = totalFiltered;
            result.data = list;
            result.extra = new
            {
                totalDownloaded,
                totalNotDownloaded,
                totalAll = totalFiltered
            };

            return result;
        }

        public void Create(DocumentRecapViewModel docRecapViewModel, UserViewModel currentUser)
        {
            var docRecap = Mapper.Map<DocumentRecapViewModel, DocumentRecap>(docRecapViewModel);

            docRecap.Id = Guid.NewGuid();
            docRecap.DateCreated = DateTime.Now;
            docRecap.CreatedById = currentUser.Id;

            db.DocumentRecaps.Add(docRecap);

            Mapper.Map(docRecap, docRecapViewModel);
        }

        public void Update(DocumentRecapViewModel docRecapViewModel, UserViewModel currentUser)
        {
            var docRecap = db.DocumentRecaps.Find(docRecapViewModel.Id);

            if (docRecap == null) throw new Exception("Dokumen tidak ditemukan.");

            Mapper.Map(docRecapViewModel, docRecap);

            docRecap.DateModified = DateTime.Now;
            docRecap.ModifiedById = currentUser.Id;

            docRecapViewModel.DateModified = docRecap.DateModified;
            docRecapViewModel.ModifiedById = docRecap.ModifiedById;
        }
    }
}