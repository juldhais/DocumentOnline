﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentOnline.Enums;
using DocumentOnline.Helpers;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Data.Entity;
using System.Linq;

namespace DocumentOnline.Logics
{
    public class InvoiceLogic
    {
        private DataContext db;

        public InvoiceLogic(DataContext db)
        {
            this.db = db;
        }

        public InvoiceViewModel Get(Guid? id)
        {
            var invoice = db.Invoices.Where(x => x.Id == id)
                            .ProjectTo<InvoiceViewModel>()
                            .FirstOrDefault();

            return invoice;
        }

        public DataTableResult GetListDataTable(Guid? userId, DataTableParameter parameter)
        {
            var searchBy = (parameter.search != null) ? parameter.search.value : null;
            var take = parameter.length;
            var skip = parameter.start;

            //TODO : implement order

            var query = db.Invoices.AsQueryable();

            //filter user
            var user = db.Users.Find(userId);
            if (user.Role == Role.SuperUser)
            {
                var listUserId = db.Users.Where(x => x.ParentId == userId)
                                    .Select(x => (Guid?)x.Id).ToList();

                listUserId.Add(userId);

                query = query.Where(x => listUserId.Contains(x.UserId));
            }
            else if (user.Role == Role.User)
                query = query.Where(x => x.UserId == userId);
            //end filter user

            var totalResult = query.Count();

            //custom filter
            if (parameter.dateColumn.IsNotEmpty())
            {
                if (parameter.dateColumn == "Date")
                    query = query.Where(x => DbFunctions.TruncateTime(x.Date) >= parameter.fromDate
                                          && DbFunctions.TruncateTime(x.Date) <= parameter.toDate);
            }

            if (parameter.filter != null)
            {
                foreach (var item in parameter.filter)
                {
                    if (item.keyword.IsEmpty() || item.column.IsEmpty()) continue;

                    if (item.column == "CompanyName")
                        query = query.Where(x => x.User.CompanyName.Contains(item.keyword));
                    else if (item.column == "BranchName")
                        query = query.Where(x => x.User.BranchName.Contains(item.keyword));
                    else if (item.column == "Status")
                        query = query.Where(x => x.Status == item.keyword);
                }
            }
            //end custom filter

            //search
            if (searchBy.IsNotEmpty())
                query = query.Where(x => x.Number.StartsWith(searchBy)
                        || x.User.Name.Contains(searchBy)
                        || x.User.CompanyName.Contains(searchBy)
                        || x.User.BranchName.Contains(searchBy));
            //end search

            var list = query.OrderByDescending(x => x.Number)
                       .Skip(skip)
                       .Take(take)
                       .ProjectTo<InvoiceViewModel>()
                       .ToList();

            //unbound
            foreach (var item in list)
            {
                item.StringDate = item.Date.ToShortDate();
            }

                var totalFiltered = list.Count();
            var totalDownloaded = query.Count(x => x.DownloadCount > 0);
            var totalNotDownloaded = query.Count(x => x.DownloadCount == 0);

            var result = new DataTableResult();
            result.draw = parameter.draw;
            result.recordsTotal = totalResult;
            result.recordsFiltered = totalFiltered;
            result.data = list;
            result.extra = new
            {
                totalDownloaded,
                totalNotDownloaded,
                totalAll = totalFiltered
            };

            return result;

        }

        public void Create(InvoiceViewModel invoiceViewModel, UserViewModel currentUser)
        {
            var invoice = Mapper.Map<InvoiceViewModel, Invoice>(invoiceViewModel);
            
            invoice.DateCreated = DateTime.Now;
            invoice.CreatedById = currentUser?.Id;

            db.Invoices.Add(invoice);

            Mapper.Map(invoice, invoiceViewModel);
        }

        public void Update(InvoiceViewModel invoiceViewModel, UserViewModel currentUser)
        {
            var invoice = db.Invoices.Find(invoiceViewModel.Id);

            if (invoice == null) throw new Exception("invoice tidak ditemukan.");

            Mapper.Map(invoiceViewModel, invoice);

            invoice.DateModified = DateTime.Now;
            invoice.ModifiedById = currentUser?.Id;

            invoiceViewModel.DateModified = invoice.DateModified;
            invoiceViewModel.ModifiedById = invoice.ModifiedById;
        }

        public void Delete(Guid? id)
        {
            var invoice = db.Invoices.Find(id);

            if(invoice == null) throw new Exception("invoice tidak ditemukan.");

            db.Invoices.Remove(invoice);
        }

        public string CreateinvoiceNumber()
        {
            var year = DateTime.Now.Year;
            var month = DateTime.Now.Month;
            var prefix = year.ToString().Substring(2, 2) + month.ToString("00");

            var lastNumber = db.Invoices.Where(x => x.Number.StartsWith(prefix))
                             .OrderByDescending(x => x.Number)
                             .Select(x => x.Number)
                             .FirstOrDefault();

            if (lastNumber == null) return prefix + "000001";

            var lastSeries = lastNumber.Substring(prefix.Length, 6);
            var newSeries = lastSeries.ToInteger() + 1;
            var newNumber = prefix + newSeries.ToString("000000");

            return newNumber;
        }
    }
}