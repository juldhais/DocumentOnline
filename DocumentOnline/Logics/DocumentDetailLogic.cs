﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocumentOnline.Logics
{
    public class DocumentDetailLogic
    {
        private DataContext db;

        public DocumentDetailLogic(DataContext db)
        {
            this.db = db;
        }

        public DocumentDetailViewModel Get(Guid? id)
        {
            var docDetail = db.DocumentDetails.Find(id);

            var docDetailViewModel = Mapper.Map<DocumentDetail, DocumentDetailViewModel>(docDetail);

            return docDetailViewModel;
        }

        public DocumentDetailViewModel Get(Guid? documentId, string docType)
        {
            var docDetail = db.DocumentDetails.Where(x => x.DocumentId == documentId
                                                    && x.DocumentType.Name == docType)
                                              .ProjectTo<DocumentDetailViewModel>()
                                              .FirstOrDefault();
            return docDetail;
        }

        public void Create(DocumentDetailViewModel docDetailViewModel, UserViewModel currentUser)
        {
            var docDetail = Mapper.Map<DocumentDetailViewModel, DocumentDetail>(docDetailViewModel);

            docDetail.Id = Guid.NewGuid();
            docDetail.DateCreated = DateTime.Now;
            docDetail.CreatedById = currentUser?.Id;
            db.DocumentDetails.Add(docDetail);

            Mapper.Map(docDetail, docDetailViewModel);
        }

        public void Update(DocumentDetailViewModel docDetailViewModel, UserViewModel currentUser)
        {
            var docDetail = db.DocumentDetails.Find(docDetailViewModel.Id);

            if (docDetail == null) throw new Exception("Dokumen detail tidak ditemukan.");

            Mapper.Map(docDetailViewModel, docDetail);

            docDetail.DateModified = DateTime.Now;
            docDetail.ModifiedById = currentUser?.Id;

            docDetailViewModel.DateModified = docDetail.DateModified;
            docDetailViewModel.ModifiedById = docDetail.ModifiedById;
        }

        public void Delete(Guid? id, UserViewModel currentUser)
        {
            var docDetail = db.DocumentDetails.Find(id);
            db.DocumentDetails.Remove(docDetail);
        }

        public List<DocumentDetailViewModel> GetList(Guid? documentId)
        {
            var list = db.DocumentDetails.Where(x => x.DocumentId == documentId)
                        .ProjectTo<DocumentDetailViewModel>()
                        .ToList();

            return list;
        }
    }
}