﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentOnline.Enums;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DocumentOnline.Logics
{
    public class NotificationLogic
    {
        private DataContext db;

        public NotificationLogic(DataContext db)
        {
            this.db = db;
        }

        public NotificationViewModel Get(Guid id)
        {
            var notification = db.Notifications.Find(id);

            var viewModel = Mapper.Map<Notification, NotificationViewModel>(notification);

            return viewModel;
        }

        public void Create(NotificationViewModel notificationViewModel)
        {
            var notification = Mapper.Map<NotificationViewModel, Notification>(notificationViewModel);

            notification.Id = Guid.NewGuid();

            db.Notifications.Add(notification);

            Mapper.Map(notification, notificationViewModel);
        }

        public void Read(Guid? notificationId)
        {
            var notification = db.Notifications.Find(notificationId);

            if (notification == null)
                throw new Exception("Data tidak ditemukan.");

            notification.IsRead = true;
        }

        public List<NotificationViewModel> GetListUnreadNotification(Guid? userId)
        {
            var list = db.Notifications.Where(x => x.UserId == userId && x.IsRead == false)
                        .ProjectTo<NotificationViewModel>()
                        .ToList();

            return list;
        }

        public List<string> GetListStringUnreadNotification(UserViewModel currentUser)
        {
            Guid? userId = null;
            if (currentUser.Role == Role.SuperAdmin || currentUser.Role == Role.Admin)
                userId = currentUser.Id;

            var list = db.Notifications.Where(x => x.UserId == userId && x.IsRead == false)
                        .Select(x => x.Message)
                        .ToList();

            return list;
        }

        public List<NotificationViewModel> GetListNotification(Guid? userId, DateTime startDate, DateTime endDate)
        {
            var list = db.Notifications.Where(x => x.UserId == userId 
                                        && DbFunctions.TruncateTime(x.Date) >= startDate
                                        && DbFunctions.TruncateTime(x.Date) <= endDate)
                        .ProjectTo<NotificationViewModel>()
                        .ToList();

            return list;
        }
    }
}