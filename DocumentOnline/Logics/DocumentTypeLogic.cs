﻿using AutoMapper;
using DocumentOnline.Models;
using DocumentOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;

namespace DocumentOnline.Logics
{
    public class DocumentTypeLogic
    {
        private DataContext db;

        public DocumentTypeLogic(DataContext db)
        {
            this.db = db;
        }

        public DocumentTypeViewModel Get(Guid? id)
        {
            var docType = db.DocumentTypes.Find(id);
            
            var docTypeViewModel = Mapper.Map<DocumentType, DocumentTypeViewModel>(docType);

            return docTypeViewModel;
        }

        public List<DocumentTypeViewModel> GetList()
        {
            var list = db.DocumentTypes.ProjectTo<DocumentTypeViewModel>().ToList();
            return list;
        }

        public DocumentTypeViewModel GetByName(string name)
        {
            DocumentType docType;

            //read first letter
            if (name.Length == 1)
                docType = db.DocumentTypes.FirstOrDefault(x => x.Name.Substring(0, 1) == name);
            else
                docType = db.DocumentTypes.FirstOrDefault(x => x.Name == name);

            var docTypeViewModel = Mapper.Map<DocumentType, DocumentTypeViewModel>(docType);
            return docTypeViewModel;
        }

        public void Create(DocumentTypeViewModel docTypeViewModel, UserViewModel currentUser)
        {
            var docType = Mapper.Map<DocumentType>(docTypeViewModel);

            docType.Id = Guid.NewGuid();
            docType.CreatedById = currentUser?.Id;
            docType.DateCreated = DateTime.Now;

            db.DocumentTypes.Add(docType);

            Mapper.Map(docType, docTypeViewModel);
        }

        public void Update(DocumentTypeViewModel docTypeViewModel, UserViewModel currentUser)
        {
            var docType = db.DocumentTypes.Find(docTypeViewModel.Id);

            if (docType == null) throw new Exception("Dokumen type tidak ditemukan.");

            Mapper.Map(docTypeViewModel, docType);

            docType.DateModified = DateTime.Now;
            docType.ModifiedById = currentUser?.Id;

            Mapper.Map(docType, docTypeViewModel);
        }
    }
}