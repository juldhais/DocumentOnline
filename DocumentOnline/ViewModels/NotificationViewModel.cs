﻿using System;

namespace DocumentOnline.ViewModels
{
    public class NotificationViewModel
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
    }
}