﻿using System;

namespace DocumentOnline.ViewModels
{
    public class DocumentTypeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public Guid? ModifiedById { get; set; }
        public string ModifiedByName { get; set; }
    }
}