﻿using System;

namespace DocumentOnline.ViewModels
{
    public class DocumentRevisionViewModel
    {
        public Guid Id { get; set; }
        public Guid? DocumentId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public string Remarks { get; set; }
        public string UserDocumentFolder { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public Guid? ModifiedById { get; set; }
        public string ModifiedByName { get; set; }

        public int DownloadCount { get; set; }
    }
}