﻿using System;

namespace DocumentOnline.ViewModels
{
    public class InvoiceViewModel
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public string UserName { get; set; }
        public string UserDocumentFolder { get; set; }
        public string UserCompanyName { get; set; }
        public string UserBranchName { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string Remarks { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public string AttachmentFileName { get; set; }
        public string AttachmentSystemFileName { get; set; }
        public int DownloadCount { get; set; }
        public string Status { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public Guid? ModifiedById { get; set; }
        public string ModifiedByName { get; set; }

        //unbound
        public string StringDate { get; set; }
    }
}