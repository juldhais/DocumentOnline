﻿using System;

namespace DocumentOnline.ViewModels
{
    public class IdNameViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}