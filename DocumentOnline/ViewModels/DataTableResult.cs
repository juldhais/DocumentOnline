﻿using System;

namespace DocumentOnline.ViewModels
{
    public class DataTableResult
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public object data { get; set; }

        public object extra { get; set; }
    }
}