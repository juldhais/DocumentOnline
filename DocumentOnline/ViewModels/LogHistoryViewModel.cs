﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentOnline.ViewModels
{
    public class LogHistoryViewModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public Guid? UserId { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public string IP { get; set; }
        public string Browser { get; set; }
    }
}