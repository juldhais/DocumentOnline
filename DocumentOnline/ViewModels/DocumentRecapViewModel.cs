﻿using System;

namespace DocumentOnline.ViewModels
{
    public class DocumentRecapViewModel
    {
        public Guid Id { get; set; }
        public Guid? FromUserId { get; set; }
        public string FromUserName { get; set; }
        public string FromUserCompanyName { get; set; }
        public string FromUserBranchName { get; set; }
        public Guid? ToUserId { get; set; }
        public string ToUserName { get; set; }
        public string ToUserCompanyName { get; set; }
        public string ToUserBranchName { get; set; }
        public string ToUserDocumentFolder { get; set; }
        public DateTime Date { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public int DownloadCount { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public Guid? ModifiedById { get; set; }
        public string ModifiedByName { get; set; }

        public string StringDate { get; set; }
    }
}