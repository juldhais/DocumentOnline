﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DocumentOnline.ViewModels
{
    public class DocumentViewModel
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public string UserName { get; set; }
        public string UserCompanyName { get; set; }
        public string UserBranchName { get; set; }
        public string UserDocumentFolder { get; set; }
        public string Number { get; set; }

        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string OriginalFileName { get; set; }
        public string SystemFileName { get; set; }
        public string CustomerName { get; set; }
        public string ContractNumber { get; set; }
        public DateTime? ContractDate { get; set; }
        public string RegistrationNumber { get; set; }
        public string AktaNumber { get; set; }
        public DateTime? AktaDate { get; set; }
        public string SertifikatNumber { get; set; }
        public DateTime? SertifikatDate { get; set; }
        public string BillID { get; set; }
        public string Notaris { get; set; }

        public Guid? OpenById { get; set; }
        public string OpenByName { get; set; }
        public Guid? InProgressById { get; set; }
        public string InProgressByName { get; set; }
        public Guid? DoneById { get; set; }
        public string DoneByName { get; set; }
        public Guid? ReOpenById { get; set; }
        public string ReOpenByName { get; set; }
        public Guid? CancelById { get; set; }
        public string CancelByName { get; set; }

        public DateTime? OpenDate { get; set; }
        public DateTime? InProgressDate { get; set; }
        public DateTime? DoneDate { get; set; }
        public DateTime? ReOpenDate { get; set; }
        public DateTime? CancelDate { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public Guid? CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public Guid? ModifiedById { get; set; }
        public string ModifiedByName { get; set; }

        public int DownloadCount { get; set; }

        //unbound
        public string StringDate { get; set; }
        public string StringAktaDate { get; set; }
        public string StringContractDate { get; set; }
        public string StringSertifikatDate { get; set; }

        public string DetailList { get; set; }
        public List<IdNameViewModel> RevisionList { get; set; }
    }
}